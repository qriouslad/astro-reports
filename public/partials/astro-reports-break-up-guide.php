<?php

// The Break-Up Guide (Me and My Ex: What Went Wrong)

$report_name = 'break-up-guide';

include 'common/report-common-two-person.php';

if ( empty ( $fn ) || empty ( $id ) || empty ( $fn2 ) || empty ( $id2 ) ) {

	include 'common/report-not-found.php';

} else {

	include 'common/report-common-site-date-info.php';

	include 'common/report-common-variation.php';

	$me_and_my_ex_data_cachekey = 'mme_'.$atts['variation'].'_'.$id.'_'.$id2;

	$me_and_my_ex_data_cached = simplexml_load_string( get_transient( $me_and_my_ex_data_cachekey ) );

	if ( false === $me_and_my_ex_data_cached ) {

		$url = "https://www.zdki.us/taReportsw/MakeReport.aspx?ReportID=44&ReportVariation=".$report_variation."&ReportFormat=XML&Name1=".$fn."&BirthDate1=".$datetime."&Name2=".$fn2."&BirthDate2=".$datetime2."&StartDate=&Length=&AccountID=".$accountid."&AppID=astroreports_NUM&MemberID=numerologist1&V=2";

		// defines $data variable with API's response
		include 'common/report-common-http-auth.php';

		$report_content_raw = simplexml_load_string($data);

		set_transient( $me_and_my_ex_data_cachekey, $report_content_raw->asXML(), MONTH_IN_SECONDS );

		// echo '<h6>Uncached data is used</h6>';

	} else {

		$report_content_raw = $me_and_my_ex_data_cached;

		// echo '<h6>Cached data is used. Cache key: '.$me_and_my_ex_data_cachekey.'</h6>';

	}

	$friction = $report_content_raw->summary[0]->squareindex;
	$differences = $report_content_raw->summary[0]->oppositionindex;
	$karma = $report_content_raw->summary[0]->quincunxindex;

	$vbar1_score = $friction;
	$vbar2_score = $differences;
	$vbar3_score = $karma;

	// $friction_padded = sprintf("%02d", $friction);
	// $differences_padded = sprintf("%02d", $differences);
	// $karma_padded = sprintf("%02d", $karma);

	// echo '<h4 class="report__subtitle">Prepared for '.urldecode( $fn ).', born on '.$dateformatted.', and '.urldecode( $fn2 ).', born on '.$dateformatted2.'</h4>';

	// 'free' or 'full'
	include 'common/report-common-report-version.php';

	// ============ Report Charts ============

	if ( $report_part == 'charts' ) {

		if ( $report_version == 'full' ) {


			echo '<div class="report__chart-section">';
				echo '<h3 class="report__chart-title"><span>Friction, Differences, and Karma Meters</span></h3>';
				echo '<div class="report__chart-section-wrapper">';

					echo '<div class="report__chart-description">';
						echo '<div class="report__chart-description-wrapper">';
							echo "<p>The Friction, Differences and Karma Meters: remember how perfect things seemed back in the beginning? Or maybe you can't even recall those glorious early days, when the future looked rosy and the possibilities for your relationship seemed limitless. What was it that broke the two of you up, anyway?</p><p>The answer to that question is complex and multilayered, to be sure. But your Friction, Differences and Karma meters give you a quick, at-a-glance view of the strife in your relationship -- all the sources of irritation, stress and hostility that ultimately led to that final goodbye.</p>";
						echo '</div>';
					echo '</div>';
					echo '<div class="report__chart-visuals">';
						echo '<div class="report__chart-animation">';
							include 'common/report-chart-vertical-three-bars.php';
						echo '</div>';
						echo '<div class="report__chart-legends report__chart-legends--thirds">';

							echo '<div class="report__chart-legends-third-left">';
							    echo '<h4 class="report__chart-legends-title report__chart-legends-title--third report__chart-legends-title-third-left">Friction = '.$friction.'</h4>';
								echo '<p><span class="report__chart-legends-score">7-10</span>: Ouch -- you might still be bitter</p>';
								echo '<p><span class="report__chart-legends-score">4-6</span>: You both knew which buttons to push</p>';
								echo '<p><span class="report__chart-legends-score">1-3</span>: The Problem? A lack of passion.</p>';
							echo '</div>';

							echo '<div class="report__chart-legends-third-center">';
							    echo '<h4 class="report__chart-legends-title report__chart-legends-title--third report__chart-legends-title-third-center">Differences = '.$differences.'</h4>';
								echo '<p><span class="report__chart-legends-score">7-10</span>: You never got each other from day one.</p>';
								echo '<p><span class="report__chart-legends-score">4-6</span>: Your differences were eye-opening... until they got annoying!</p>';
								echo '<p><span class="report__chart-legends-score">1-3</span>: You could still be friends</p>';
							echo '</div>';

							echo '<div class="report__chart-legends-third-right">';
							    echo '<h4 class="report__chart-legends-title report__chart-legends-title--third report__chart-legends-title-third-right">Karma = '.$karma.'</h4>';
								echo '<p><span class="report__chart-legends-score">7-10</span>: It was doomed from the start.</p>';
								echo '<p><span class="report__chart-legends-score">4-6</span>: Unconsciously, you both acted out old issues.</p>';
								echo '<p><span class="report__chart-legends-score">1-3</span>: At least you made new mistakes instead of repeating old ones.</p>';
							echo '</div>';

						echo '</div>';
					echo '</div>';

				echo '</div>';
			echo '</div>';

		} else {

			include 'common/report-common-charts-preview.php';

		}

	} else {}

	// ============ Report Articles ============

	if ( $report_part == 'articles' ) {

		include 'common/report-free-limits-start.php';

		foreach ( $report_content_raw->detail[0]->section as $section ) {

			if ( $report_version == 'free' ) {

				if ( $i == $limit ) break;

			} else {}

			echo '<div class="report__section">';
			echo '<h2 class="report__section-title"><span>'.$section->title.'</span></h2>';

			foreach ($section->paragraph as $paragraph) {

				include 'common/report-common-content-interaspect.php';

				$heading_lower = strtolower($heading);

				if ( $planet1 == 'north' ) {

					$planet1 = 'northnode';
					$aspect = strtolower($interaspctchunk[2]);
					$planet2 = strtolower($interaspctchunk[3]);

				}

				include 'common/report-image-pool-partial.php';

				echo '<div class="report__article">';

				if ( empty($planet1) ) {

					if ( $heading_lower == 'few challenges equaled little growth' ) {

						$interaspect = 'No squares';
						$planet1 = 'square';

					} elseif ( $heading_lower == 'needed to find a wider perspective' ) {

						$interaspect = 'No oppositions';
						$planet1 = 'opposite';

					} elseif ( $heading_lower == 'together because you wanted to be' ) {

						$interaspect = 'No quincunxes';
						$planet1 = 'quincunx';

					} elseif ( $heading_lower == "not enough glue" ) {

						$interaspect = 'No conjunctions';
						$planet1 = 'conjunct';

					}

					echo '<span class="report__article-subtitle"><img src="/wp-content/uploads/images/glyphs/unaspected_white.png" /><img src="/wp-content/uploads/images/glyphs/'.$planet1.'_white.png" />'.$interaspect.'</span>';

					echo '<h4 class="report__article-title">'.$heading.'</h4>';

					include 'common/report-common-variation-content.php';

				} else {

					if ( $aspect == 'in' ) {

					echo '<span class="report__article-subtitle"><img src="/wp-content/uploads/images/glyphs/'.$planet1.'_white.png" /><img src="/wp-content/uploads/images/glyphs/'.$planet2.'_white.png" />'.$interaspect.'</span>';

					echo '<h4 class="report__article-title">'.$heading.'</h4>';

					} else {

					echo '<span class="report__article-subtitle"><img src="/wp-content/uploads/images/glyphs/'.$planet1.'_white.png" /><img src="/wp-content/uploads/images/glyphs/'.$aspect.'_white.png" /><img src="/wp-content/uploads/images/glyphs/'.$planet2.'_white.png" />'.$interaspect.'</span>';

					echo '<h4 class="report__article-title">'.$heading.'</h4>';

					}

					include 'common/report-common-variation-content.php';

			    }

				echo '</div>';

			}

			echo '</div>';

			include 'common/report-free-limits-end.php';

		}

	} else {}

}