<?php

// set namespace
use phpformbuilder\Form;

$site_url = get_site_url();

// Get global WP post object
global $post;
$report_slug = $post->post_name;

// start session
// session_start();

$method = 'POST';
$url = $site_url.'/report/free/'.$report_slug.'/';

// instanciate form and add fields
$form = new Form('personal-info', 'horizontal', 'novalidate', 'material');
$form->addPlugin('materialize', '#personal-info');

$form->setMethod($method);
$form->setAction($url, [$add_get_vars = true]);

$form->addHtml('<h4 class="astroform__section-title">Delivery Information</h4>');
$form->addInput('text', 'atv-report-type', 'free', '', 'hidden');
$form->addInput('text', 'atv-first-name', '', 'First Name ', 'required');
$form->addInput('email', 'atv-email', '', 'Email ', 'required');
$form->addHtml('<h4 class="astroform__section-title">Details Needed For Your Astrology Report</h4>');

// First person's Date of Birth fields

if ( ( $report_slug == 'romantic-compatibility' ) || ( $report_slug == 'karmic-compatibility' ) || ( $report_slug == 'friendship-compatibility' ) || ( $report_slug == 'relationship-analysis' ) || ( $report_slug == 'romantic-forecast-couples' ) || ( $report_slug == 'love-hurts' ) || ( $report_slug == 'break-up-guide' ) ) {

	$form->addHtml('<p class="astroform__input-label">Your Date of Birth<sup>*</sup></p>');

} else {

	$form->addHtml('<p class="astroform__input-label">Date of Birth<sup>*</sup></p>');

}

$form->groupInputs('atv-birth-month', 'atv-birth-day', 'atv-birth-year');

$form->setCols(0, 4);
$form->addOption('atv-birth-month', '', 'Month', '', 'selected=selected');
$form->addOption('atv-birth-month', '01', 'January');
$form->addOption('atv-birth-month', '02', 'February');
$form->addOption('atv-birth-month', '03', 'March');
$form->addOption('atv-birth-month', '04', 'April');
$form->addOption('atv-birth-month', '05', 'May');
$form->addOption('atv-birth-month', '06', 'June');
$form->addOption('atv-birth-month', '07', 'July');
$form->addOption('atv-birth-month', '08', 'August');
$form->addOption('atv-birth-month', '09', 'September');
$form->addOption('atv-birth-month', '10', 'October');
$form->addOption('atv-birth-month', '11', 'Novermber');
$form->addOption('atv-birth-month', '12', 'December');
$form->addSelect('atv-birth-month', '', 'required');

$form->setCols(0, 4);
$form->addOption('atv-birth-day', '', 'Day', '', 'selected=selected');
for ($i=1; $i < 32; $i++) {
	$form->addOption('atv-birth-day', str_pad($i, 2, "0", STR_PAD_LEFT), $i);
}
$form->addSelect('atv-birth-day', '', 'required');

$form->setCols(0, 4);
$form->addOption('atv-birth-year', '', 'Year', '', 'selected=selected');
for ($i=1919; $i < (date('Y')+1); $i++) {
	$form->addOption('atv-birth-year', $i, $i);
}
$form->addSelect('atv-birth-year', '', 'required');

// Fields shown for two person report
if ( ( $report_slug == 'romantic-compatibility' ) || ( $report_slug == 'karmic-compatibility' ) || ( $report_slug == 'friendship-compatibility' ) || ( $report_slug == 'relationship-analysis' ) || ( $report_slug == 'romantic-forecast-couples' ) || ( $report_slug == 'love-hurts' ) || ( $report_slug == 'break-up-guide' ) ) {

	// Show second person's first name input field
	$form->setCols(0, 12);
	$form->addInput('text', 'atv-first-name-2', '', 'His/Her First Name ', 'required');

	// Show second person's Date of Birth fields on reports for two people
	$form->addHtml('<p class="astroform__input-label">His/Her Date of Birth<sup>*</sup></p>');
	$form->groupInputs('atv-birth-month-2', 'atv-birth-day-2', 'atv-birth-year-2');

	$form->setCols(0, 4);
	$form->addOption('atv-birth-month-2', '', 'Month', '', 'selected=selected');
	$form->addOption('atv-birth-month-2', '01', 'January');
	$form->addOption('atv-birth-month-2', '02', 'February');
	$form->addOption('atv-birth-month-2', '03', 'March');
	$form->addOption('atv-birth-month-2', '04', 'April');
	$form->addOption('atv-birth-month-2', '05', 'May');
	$form->addOption('atv-birth-month-2', '06', 'June');
	$form->addOption('atv-birth-month-2', '07', 'July');
	$form->addOption('atv-birth-month-2', '08', 'August');
	$form->addOption('atv-birth-month-2', '09', 'September');
	$form->addOption('atv-birth-month-2', '10', 'October');
	$form->addOption('atv-birth-month-2', '11', 'Novermber');
	$form->addOption('atv-birth-month-2', '12', 'December');
	$form->addSelect('atv-birth-month-2', '', 'required');

	$form->setCols(0, 4);
	$form->addOption('atv-birth-day-2', '', 'Day', '', 'selected=selected');
	for ($i=1; $i < 32; $i++) {
		$form->addOption('atv-birth-day-2', $i, $i);
	}
	$form->addSelect('atv-birth-day-2', '', 'required');

	$form->setCols(0, 4);
	$form->addOption('atv-birth-year-2', '', 'Year', '', 'selected=selected');
	for ($i=1919; $i < (date('Y')+1); $i++) {
		$form->addOption('atv-birth-year-2', $i, $i);
	}
	$form->addSelect('atv-birth-year-2', '', 'required');

}

// $form->addInput('date', 'atv-dob', '', 'Date of Birth', 'required');

if ( ( $report_slug == 'romantic-compatibility' ) || ( $report_slug == 'karmic-compatibility' ) || ( $report_slug == 'friendship-compatibility' ) || ( $report_slug == 'relationship-analysis' ) || ( $report_slug == 'romantic-forecast-couples' ) || ( $report_slug == 'love-hurts' ) || ( $report_slug == 'break-up-guide' ) ) {

	$form->addHtml('<p class="astroform__section-gdpr-desc">Your name and the dates of birth are used to customize your astrology report.<br /> <a href="https://astrology.tv/privacy-policy/">Privacy Policy</a></p>');

} else {

	$form->addHtml('<p class="astroform__section-gdpr-desc">Your name and date of birth are used to customize your astrology report.<br /> <a href="https://astrology.tv/privacy-policy/">Privacy Policy</a></p>');

}

$form->addBtn('submit', 'atv-personal-info-submit', 1, 'Get My Free Report', 'class=primary-cta');

$form->render();

$form->printJsCode();