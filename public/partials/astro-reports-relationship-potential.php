<?php

$report_name = 'relationship-potential';

include 'common/report-common-two-person.php';

if ( empty ( $fn ) || empty ( $id ) || empty ( $fn2 ) || empty ( $id2 ) ) {

	include 'common/report-not-found.php';

} else {

	include 'common/report-common-site-date-info.php';

	include 'common/report-common-variation.php';

	$relationship_analysis_data_cachekey = 'ra_'.$atts['variation'].'_'.$id.'_'.$id2;

	$relationship_analysis_data_cached = simplexml_load_string( get_transient( $relationship_analysis_data_cachekey ) );

	if ( false === $relationship_analysis_data_cached ) {

		$url = "https://www.zdki.us/taReportsw/MakeReport.aspx?ReportID=49&ReportVariation=".$report_variation."&ReportFormat=XML&Name1=".$fn."&BirthDate1=".$datetime."&Name2=".$fn2."&BirthDate2=".$datetime2."&StartDate=&Length=&AccountID=".$accountid."&AppID=astroreports_NUM&MemberID=numerologist1&V=2";

		// defines $data variable with API's response
		include 'common/report-common-http-auth.php';

		$report_content_raw = simplexml_load_string($data);

		set_transient( $relationship_analysis_data_cachekey, $report_content_raw->asXML(), MONTH_IN_SECONDS );

		// echo '<h6>Uncached data is used</h6>';

	} else {

		$report_content_raw = $relationship_analysis_data_cached;

		// echo '<h6>Cached data is used. Cache key: '.$relationship_analysis_data_cachekey.'</h6>';

	}

	$romantic_nature = $report_content_raw->summary[0]->venusindex;
	$passionate_nature = $report_content_raw->summary[0]->marsindex;

	$vbar1_score = $romantic_nature;
	$vbar2_score = $passionate_nature;

	// $romantic_nature_padded = sprintf("%02d", $romantic_nature);
	// $passionate_nature_padded = sprintf("%02d", $passionate_nature);

	// echo '<h4 class="report__subtitle">Prepared for '.urldecode( $fn ).', born on '.$dateformatted.', and '.urldecode( $fn2 ).', born on '.$dateformatted2.'</h4>';

	// 'free' or 'full'
	include 'common/report-common-report-version.php';

	// ============ Report Charts ============

	if ( $report_part == 'charts' ) {

		if ( $report_version == 'full' ) {

		    echo '<div class="report__chart-section">';
			    echo '<h3 class="report__chart-title"><span>The Overview of your Relationship</span></h3>';
				echo '<div class="report__chart-section-wrapper">';

					echo '<div class="report__chart-description">';
						echo '<div class="report__chart-description-wrapper">';
						    echo "<p>The Romance and Passion Meters: love can be complex but The Astrologer can break it down! Our special Romance and Passion Meters reads the composite chart between you and your sweetie, and reveals the levels of romance and passion in your relationship. Now you'll know at a glance whether things will be smooth sailing, an uphill battle -- or a roller coaster ride between you both.</p><p>Before you jump in, there's a trick to reading the meters. You might think you want a lot of sweet stuff and very little spice, but what you really want is a balance between the two. Just imagine: With all good feelings, pleasant times and nothing to break up the monotony, things would get pretty dull (yawn). And too much passion can also ruin a relationship; without a little breathing room between fights and lusty encounters, you'd tire each other out, fast! Something in the middle is just the ticket. A healthy dose of passion means strong physical attraction plus a good argument every now and then to clear the air, and a sense of trust and understanding means you can always kiss and make up.</p><p>One more word of caution before you get started: Remember, no relationship is perfect. Even with what seems like the ideal balance of romance and passion, an affair could turn out to be a total flop. For a much more in-depth reading of the inner workings of your bond, read your full Relationship Analysis for Couples report from The Astrologer.</p>";
						echo '</div>';
					echo '</div>';
					echo '<div class="report__chart-visuals">';
						echo '<div class="report__chart-animation">';
							include 'common/report-chart-vertical-two-bars.php';
						echo '</div>';
						echo '<div class="report__chart-legends">';

							echo '<div class="report__chart-legends-half-left">';
							    echo '<h4 class="report__chart-legends-title report__chart-legends-title--half-left"">Romance = '.$romantic_nature.'</h4>';
					            echo '<p><strong>7-10</strong> Over-the-top: diamonds and heartflet declarations</p>';
					            echo '<p><strong>4-6</strong> Indulgent: flowers, chocolates, back rubs</p>';
					            echo '<p><strong>1-3</strong> Thrify: the occational rose</p>';
							echo '</div>';

							echo '<div class="report__chart-legends-half-right">';
							    echo '<h4 class="report__chart-legends-title report__chart-legends-title--half-right">Passion = '.$passionate_nature.'</h4>';
					            echo '<p><strong>7-10</strong>: Revved and ready, morning and night.</p>';
					            echo '<p><strong>4-6</strong>: Up for it almost anytime</p>';
					            echo '<p><strong>1-3</strong>: A bit shy and reserved</p>';
							echo '</div>';

						echo '</div>';		
				    echo '</div>';

			    echo '</div>';
		    echo '</div>';

		} else {

			include 'common/report-common-charts-preview.php';

		}

	} else {}

	// ============ Report Articles ============

	if ( $report_part == 'articles' ) {

		include 'common/report-free-limits-start.php';

		foreach ( $report_content_raw->detail[0]->section as $section ) {

			if ( $report_version == 'free' ) {

				if ( $i == $limit ) break;

			} else {}

			$section_title = $section->title;
			$section_title_chunk = explode(" ",$section_title);
			$section_title1 = strtolower($section_title_chunk[0]);
			$section_title1 = str_replace(":","",$section_title1);  

			echo '<div class="report__section">';
			echo '<h2 class="report__section-title"><span><img src="/wp-content/uploads/images/glyphs/'.$section_title1.'_white.png" />'.$section_title.'</span></h2>';

			foreach ( $section->paragraph as $paragraph ) {

				include 'common/report-common-content-interaspect.php';

				$heading_lower = strtolower($heading);

				if ( $planet1 == 'north' ) {

					$planet1 = 'northnode';
					$aspect = strtolower($interaspctchunk[2]);
					$planet2 = strtolower($interaspctchunk[3]);

				}

				include 'common/report-image-pool-complete.php';

				echo '<div class="report__article">';

				$idstring = $planet1;
				$idstring .= $aspect;
				$idstring .= $planet2;

				if ( empty( $idstring ) ) {

					if ( $heading_lower == 'a couple of faith' ) {

						$interaspect = 'Jupiter unaspected';
						$planet1 = 'jupiter';
						$interaspect_slug = 'jupiter-unaspected';

					} elseif ( $heading_lower == 'intense drive' ) {

						$interaspect = 'Mars unaspected';
						$planet1 = 'mars';
						$interaspect_slug = 'mars-unaspected';

					} elseif ( $heading_lower == "completing each other's sentences" ) {

						$interaspect = 'Mercury unaspected';
						$planet1 = 'mercury';
						$interaspect_slug = 'mercury-unaspected';

					} elseif ( $heading_lower == 'a spiritual connection' ) {

						$interaspect = 'Neptune unaspected';
						$planet1 = 'neptune';
						$interaspect_slug = 'neptune-unaspected';

					} elseif ( $heading_lower == 'power players' ) {

						$interaspect = 'Pluto unaspected';
						$planet1 = 'pluto';
						$interaspect_slug = 'pluto-unaspected';

					} elseif ( $heading_lower == 'no nonsense' ) {

						$interaspect = 'Saturn unaspected';
						$planet1 = 'saturn';
						$interaspect_slug = 'saturn-unaspected';

					} elseif ( $heading_lower == 'an independent unit' ) {

						$interaspect = 'Sun unaspected';
						$planet1 = 'sun';
						$interaspect_slug = 'sun-unaspected';

					} elseif ( $heading_lower == 'a unique couple' ) {

						$interaspect = 'Uranus unaspected';
						$planet1 = 'uranus';
						$interaspect_slug = 'uranus-unaspected';

					} elseif ( $heading_lower == 'a well-connected couple' ) {

						$interaspect = 'Venus unaspected';
						$planet1 = 'venus';
						$interaspect_slug = 'venus-unaspected';

					}


					echo '<span class="report__article-subtitle"><img src="/wp-content/uploads/images/glyphs/'.$planet1.'_white.png" /><img src="/wp-content/uploads/images/glyphs/unaspected_white.png" />'.$interaspect.'</span>';

					echo '<h4 class="report__article-title">'.$heading.'</h4>';

					include 'common/report-common-variation-content.php';

				} else {

					if ( $aspect == 'in' ) {

						echo '<span class="report__article-subtitle"><img src="/wp-content/uploads/images/glyphs/'.$planet1.'_white.png" /><img src="/wp-content/uploads/images/glyphs/'.$planet2.'_white.png" />'.$interaspect.'</span>';

						echo '<h4 class="report__article-title">'.$heading.'</h4>';

					} else {

						echo '<span class="report__article-subtitle"><img src="/wp-content/uploads/images/glyphs/'.$planet1.'_white.png" /><img src="/wp-content/uploads/images/glyphs/'.$aspect.'_white.png" /><img src="/wp-content/uploads/images/glyphs/'.$planet2.'_white.png" />'.$interaspect.'</span>';

						echo '<h4 class="report__article-title">'.$heading.'</h4>';

					}

					include 'common/report-common-variation-content.php';

			    }

				echo '</div>';

			}

			echo '</div>';

			include 'common/report-free-limits-end.php';

		}

	} else {}

}