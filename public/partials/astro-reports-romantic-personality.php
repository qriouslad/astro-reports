<?php

$report_name = 'romantic-personality';

include 'common/report-common-one-person.php';

if ( empty ( $fn ) || empty ( $id ) ) {

	include 'common/report-not-found.php';

} else {

	include 'common/report-common-site-date-info.php';

	include 'common/report-common-variation.php';

	$romantic_personality_profile_data_cachekey = 'rpp_'.$atts['variation'].'_'.$id;

	$romantic_personality_profile_data_cached = simplexml_load_string( get_transient( $romantic_personality_profile_data_cachekey ) );

	if ( false === $romantic_personality_profile_data_cached ) {

		$url = "https://www.zdki.us/taReportsw/MakeReport.aspx?ReportID=15&ReportVariation=".$report_variation."&ReportFormat=XML&Name1=".$fn."&BirthDate1=".$datetime."&Name2=&BirthDate2=&StartDate=&Length=&AccountID=".$accountid."&AppID=astroreports_NUM&MemberID=numerologist1&V=2";


		// defines $data variable with API's response
		include 'common/report-common-http-auth.php';

		$report_content_raw = simplexml_load_string($data);

		set_transient( $romantic_personality_profile_data_cachekey, $report_content_raw->asXML(), MONTH_IN_SECONDS );

		// echo '<h6>Uncached data is used</h6>';

	} else {

		$report_content_raw = $romantic_personality_profile_data_cached;

		// echo '<h6>Cached data is used. Cache key: '.$romantic_personality_profile_data_cachekey.'</h6>';

	}

	$romantic_nature = $report_content_raw->summary[0]->venusindex;
	$passionate_nature = $report_content_raw->summary[0]->marsindex;

	// For calibrating chart visual accuracy
	// $romantic_nature = 9;
	// $passionate_nature = 5;

	$vbar1_score = $romantic_nature;
	$vbar2_score = $passionate_nature;

	$romantic_nature_percentage = $romantic_nature * 10;
	$passionate_nature_percentage = $passionate_nature * 10;

	// $romantic_nature_padded = sprintf("%02d", $romantic_nature);
	// $passionate_nature_padded = sprintf("%02d", $passionate_nature);

	// echo '<h4 class="report__subtitle">Prepared for '.urldecode( $fn ).', born on '.$dateformatted.'</h4>';

	// 'free' or 'full'
	include 'common/report-common-report-version.php';

	// ============ Report Charts ============

	if ( $report_part == 'charts' ) {

		if ( $report_version == 'full' ) {

		    echo'<div class="report__chart-section">';
				echo '<h3 class="report__chart-title"><span>Your Nature</span></h3>';
				echo '<div class="report__chart-section-wrapper">';

					echo '<div class="report__chart-description">';
						echo '<div class="report__chart-description-wrapper">';
							echo "<p>The Romance and Passion Meters reads your birth chart and reveals the levels of romance and passion you have been gifted with in this life. Now you'll know at a glance whether things will be smooth sailing or an uphill battle when it comes to matters of the heart.</p><p>Before you jump in, there's a trick to reading the meters. You might think you want a lot of sweet stuff with the Romance meter but what you really want is a balance. Just imagine: With all good feelings, pleasant times and nothing to break up the monotony, things would get pretty dull (yawn). And too much passion can also ruin a relationship; without a little breathing room between fights and lusty encounters, you'd tire your lover out, fast! A healthy dose of passion means strong physical attraction plus a good argument every now and then.</p><p>One more word of caution before you get started: Remember, no relationship or person is perfect. Even with what seems like the ideal balance of romance and passion, an affair could turn out to be a total flop. It really all boils down to our free will.</p>";
						echo '</div>';
					echo '</div>';

					echo '<div class="report__chart-visuals">';
						echo '<div class="report__chart-animation">';
							include 'common/report-chart-vertical-two-bars.php';
						echo '</div>';
						echo '<div class="report__chart-legends">';
							echo '<div class="report__chart-legends-half-left">';
							    echo '<h4 class="report__chart-legends-title report__chart-legends-title--half-left">Romantic Nature = '.$romantic_nature.'</h4>';
								echo '<p><span class="report__chart-legends-score">7-10</span> Over-the-top: diamonds and heartflet declarations</p>';
								echo '<p><span class="report__chart-legends-score">4-6</span> Indulgent: flowers, chocolates, back rubs</p>';
								echo '<p><span class="report__chart-legends-score">1-3</span> Thrify: the occational rose</p>';
							echo '</div>';
							echo '<div class="report__chart-legends-half-right">';
							    echo '<h4 class="report__chart-legends-title report__chart-legends-title--half-right">Passionate Nature = '.$passionate_nature.'</h4>';
								echo '<p><span class="report__chart-legends-score">7-10</span>: Revved and ready, morning and night.</p>';
								echo '<p><span class="report__chart-legends-score">4-6</span>: Up for it almost anytime</p>';
								echo '<p><span class="report__chart-legends-score">1-3</span>: A bit shy and reserved</p>';
							echo '</div>';
						echo '</div>';
					echo '</div>';
				
				echo '</div>';
		    echo '</div>';

		} else {

			include 'common/report-common-charts-preview.php';

		}

	} else {}

	// ============ Report Articles ============

	if ( $report_part == 'articles' ) {

		include 'common/report-free-limits-start.php';

	    foreach ( $report_content_raw->detail[0]->section as $section ) {

			if ( $report_version == 'free' ) {

				if ( $i == $limit ) break;

			} else {}

			foreach ( $section->paragraph as $paragraph ) {

				$interaspect = $paragraph->interaspect;
				$interaspctchunk = explode(" ",$interaspect);
				$planet1 = strtolower($interaspctchunk[0]);

			}

			echo'<div class="report__section">';
			echo '<h2 class="report__section-title"><span><img src="/wp-content/uploads/images/glyphs/'.$planet1.'_white.png" />'.$section->title.'</span></h2>';

			foreach ( $section->paragraph as $paragraph ) {

				include 'common/report-common-content-interaspect.php';

				include 'common/report-image-pool-partial.php';

				echo '<div class="report__article">';

		        if ( $aspect == 'in' ) {

					echo '<span class="report__article-subtitle"><img src="/wp-content/uploads/images/glyphs/'.$planet1.'_white.png" /><img src="/wp-content/uploads/images/glyphs/'.$planet2.'_white.png" />'.$interaspect.'</span>';

					echo '<h4 class="report__article-title">'.$heading.'</h4>';

		        } else {

					echo '<span class="report__article-subtitle"><img src="/wp-content/uploads/images/glyphs/'.$planet1.'_white.png" /><img src="/wp-content/uploads/images/glyphs/'.$aspect.'_white.png" /><img src="/wp-content/uploads/images/glyphs/'.$planet2.'_white.png" />'.$interaspect.'</span>';

					echo '<h4 class="report__article-title">'.$heading.'</h4>';

		        }

				include 'common/report-common-variation-content.php';

				echo '</div>';

			}

			echo '</div>';

			include 'common/report-free-limits-end.php';

	    }

	} else {}

}