<?php

echo '<div class="report__chart-section">';

	echo '<h3 class="report__chart-title"><span>How strong is your connection?</span></h2>';

	echo '<div id="#sun" class="report__chart-section-wrapper">';

		echo '<div class="report__chart-description">';

			echo '<div class="report__chart-description-wrapper">';

				echo '<h4 class="report__chart-subtitle"><span>Wondering how the two of you match up?</span></h2>';

				echo '<p>Now you can find out, at a glance! Our two-way bar graph reveal the chemistry between you and the object of your desires -- how you affect them, and how they affect you -- giving you a quick view of the strength of your connection in six important categories.</p>';

				echo '<p><a class="report__unlock-link" href="#purchase">Unlock your full report</a></p>';

			echo '</div>';

		echo '</div>';

		echo '<div class="report__chart-visuals">';

			echo '<img class="report__chart-visuals-preview" src="/wp-content/uploads/images/charts/locked_chart.png" />';

		echo '</div>';

	echo '</div>';

echo '</div>';
