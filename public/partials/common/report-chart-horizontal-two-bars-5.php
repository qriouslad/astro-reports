<?php

echo '<div class="report__chart-horizontal-bars">';
	include 'part/report-chart-horizontal-two-bars-xnumbers.php';
	echo '<div id="hbar5primary" class="report__chart-hbar report__chart-hbar--primary"></div>';
	echo '<div id="hbar5secondary" class="report__chart-hbar report__chart-hbar--secondary"></div>';
echo '</div>';

?>

<script type="text/javascript">

	jQuery( document ).ready(function() {
	  
	  const hbar5PrimaryValue = <?php echo $hbar5_primary; ?> * 10;
	  const hbar5SecondaryValue = <?php echo $hbar5_secondary; ?> * 10;
	  
	  var hbar5Primary = document.getElementById('hbar5primary');
	  var hbar5PrimaryHeight = hbar5Primary.clientHeight;
	  
	  var hbar5Secondary = document.getElementById('hbar5secondary');
	  var hbar5SecondaryHeight = hbar5Secondary.clientHeight;
	  
	  // listen for scroll event and call animate function
	  document.addEventListener('scroll', animate);

	  // check if element is in view

	  // check if element is in view
	  function inViewhbar5Primary() {

	    // get window height
	    var windowHeight = window.innerHeight;
	    
	    // get number of pixels that the document is scrolled
	    var scrollY = window.scrollY || window.pageYOffset;

	    // get current scroll position (distance from the top of the page to the bottom of the current viewport)
	    var scrollPosition = scrollY + windowHeight;

	    // get element position (distance from the top of the page to the bottom of the element)
	    var hbar5PrimaryPositionIn = hbar5Primary.getBoundingClientRect().top + scrollY;

	    // get element position (distance from the top of the page to the bottom of the element)
	    var hbar5PrimaryPositionOut = hbar5Primary.getBoundingClientRect().top + scrollY + hbar5PrimaryHeight + windowHeight;    
	    
	    // is scroll position greater than element position? (is element in view?)
	    if ( (scrollPosition > hbar5PrimaryPositionIn) && (scrollPosition < hbar5PrimaryPositionOut) ) {
	      return true;
	    }

	    return false;
	  }

	  // check if element is in view
	  function inViewhbar5Secondary() {

	    // get window height
	    var windowHeight = window.innerHeight;
	    
	    // get number of pixels that the document is scrolled
	    var scrollY = window.scrollY || window.pageYOffset;

	    // get current scroll position (distance from the top of the page to the bottom of the current viewport)
	    var scrollPosition = scrollY + windowHeight;

	    // get element position (distance from the top of the page to the bottom of the element)
	    var hbar5SecondaryPositionIn = hbar5Secondary.getBoundingClientRect().top + scrollY;

	    // get element position (distance from the top of the page to the bottom of the element)
	    var hbar5SecondaryPositionOut = hbar5Secondary.getBoundingClientRect().top + scrollY + hbar5SecondaryHeight + windowHeight;    
	    
	    // is scroll position greater than element position? (is element in view?)
	    if ( (scrollPosition > hbar5SecondaryPositionIn) && (scrollPosition < hbar5SecondaryPositionOut) ) {
	      return true;
	    }

	    return false;
	  }
	  
	  // animate element when it is in view
	  function animate() {

	    if (inViewhbar5Primary()) {
	        hbar5Primary.classList.add('hbar'+hbar5PrimaryValue);
	        hbar5Primary.classList.add('hbar-animation');
	    } else {
	        hbar5Primary.classList.remove('hbar'+hbar5PrimaryValue);
	        hbar5Primary.classList.remove('hbar-animation');
	    }
	                         
	    if (inViewhbar5Secondary()) {
	        hbar5Secondary.classList.add('hbar'+hbar5SecondaryValue);
	        hbar5Secondary.classList.add('hbar-animation');
	    } else {
	        hbar5Secondary.classList.remove('hbar'+hbar5SecondaryValue);
	        hbar5Secondary.classList.remove('hbar-animation');
	    }

	  }
	    
	});

</script>