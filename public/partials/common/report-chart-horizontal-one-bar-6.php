<?php

echo '<div class="report__chart-horizontal-bars report__chart-horizontal-bars--one-bar">';
	include 'part/report-chart-horizontal-two-bars-xnumbers.php';
	echo '<div id="hbar6primary" class="report__chart-hbar report__chart-hbar--primary"></div>';
echo '</div>';

?>

<script type="text/javascript">

	jQuery( document ).ready(function() {
	  
	  const hbar6PrimaryValue = <?php echo $hbar6_primary; ?> * 10;
	  
	  var hbar6Primary = document.getElementById('hbar6primary');
	  var hbar6PrimaryHeight = hbar6Primary.clientHeight;
	  	  
	  // listen for scroll event and call animate function
	  document.addEventListener('scroll', animate);

	  // check if element is in view

	  // check if element is in view
	  function inViewhbar6Primary() {

	    // get window height
	    var windowHeight = window.innerHeight;
	    
	    // get number of pixels that the document is scrolled
	    var scrollY = window.scrollY || window.pageYOffset;

	    // get current scroll position (distance from the top of the page to the bottom of the current viewport)
	    var scrollPosition = scrollY + windowHeight;

	    // get element position (distance from the top of the page to the bottom of the element)
	    var hbar6PrimaryPositionIn = hbar6Primary.getBoundingClientRect().top + scrollY;

	    // get element position (distance from the top of the page to the bottom of the element)
	    var hbar6PrimaryPositionOut = hbar6Primary.getBoundingClientRect().top + scrollY + hbar6PrimaryHeight + windowHeight;    
	    
	    // is scroll position greater than element position? (is element in view?)
	    if ( (scrollPosition > hbar6PrimaryPositionIn) && (scrollPosition < hbar6PrimaryPositionOut) ) {
	      return true;
	    }

	    return false;
	  }
	  
	  // animate element when it is in view
	  function animate() {

	    if (inViewhbar6Primary()) {
	        hbar6Primary.classList.add('hbar'+hbar6PrimaryValue);
	        hbar6Primary.classList.add('hbar-animation');
	    } else {
	        hbar6Primary.classList.remove('hbar'+hbar6PrimaryValue);
	        hbar6Primary.classList.remove('hbar-animation');
	    }

	  }
	    
	});

</script>