<?php

// Check date format: http://stackoverflow.com/a/7099546
$from = $paragraph->dates->attributes()->from;

if ( $from == date('m/d/Y',strtotime($from)) ) {

	$from = date('M d',strtotime($from));

	$from = 'From '.$from;

} else {

	$from = $from;

}

$thru = $paragraph->dates->attributes()->thru;

if ( $thru == date('m/d/Y',strtotime($thru)) ) {

	$thru = date('M d',strtotime($thru));

} else {

	$thru = $thru;

}

$exact = $paragraph->dates->attributes()->peak;

if ( $exact == date('m/d/Y',strtotime($exact)) ) {

	$exact = date('M d, Y',strtotime($exact));

} else {

	$exact = $exact;

}

if ( !empty($exact) ) {

	echo '<p class="report_article-dates"><em>'.$from.' To '.$thru.'</em>&nbsp;&nbsp; Exact: '.$exact.'</p>';

} else {

	echo '<p class="report_article-dates"><em>'.$from.' To '.$thru.'</em></p>';

}
