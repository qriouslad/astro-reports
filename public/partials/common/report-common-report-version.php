<?php

// Expecting 'free' or empty. From /info/ data entry form as hidden field.
$report_version = $_POST['atv-report-type'];

$permalink = get_permalink();

if ( ( strstr( $permalink, 'free' ) ) || ( !empty( $report_version ) ) || ( $atts['variation'] == 'short' ) ) {
	
	$report_version = 'free';

} else {

	$report_version  = 'full';

}
