<?php

echo '<div class="report__chart-hbar-legends">';
	echo '<div class="report__chart-hbar-legend-item">';
		echo '<div class="report__chart-hbar-legend-text">Your connection to them</div>';
		echo '<div class="report__chart-hbar-legend-icon report__chart-hbar-legend-icon--primary"></div>';
	echo '</div>';
	echo '<div class="report__chart-hbar-legend-item">';
		echo '<div class="report__chart-hbar-legend-text">Their connection to you</div>';
		echo '<div class="report__chart-hbar-legend-icon report__chart-hbar-legend-icon--secondary"></div>';
	echo '</div>';
echo '</div>';