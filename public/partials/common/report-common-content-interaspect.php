<?php

	$heading = $paragraph->heading;
	$text = $paragraph->text;

	if ( ! empty( $paragraph->transit ) ) {

		$interaspect = $paragraph->transit;

	} elseif ( ! empty( $paragraph->interaspect ) ){

		$interaspect = $paragraph->interaspect;

	} else {}

	$interaspect = str_replace("opposition","opposite",$interaspect);
	$interaspect = str_replace("North Node","Northnode",$interaspect);

	$interaspectchunk = explode(" ",$interaspect);
	$planet1 = (string)strtolower($interaspectchunk[0]);
	$aspect = (string)strtolower($interaspectchunk[1]);
	$planet2 = (string)strtolower($interaspectchunk[2]);
