<?php

echo '<div class="report__chart-horizontal-bars report__chart-horizontal-bars--one-bar">';
	include 'part/report-chart-horizontal-two-bars-xnumbers.php';
	echo '<div id="hbar5primary" class="report__chart-hbar report__chart-hbar--primary"></div>';
echo '</div>';

?>

<script type="text/javascript">

	jQuery( document ).ready(function() {
	  
	  const hbar5PrimaryValue = <?php echo $hbar5_primary; ?> * 10;
	  
	  var hbar5Primary = document.getElementById('hbar5primary');
	  var hbar5PrimaryHeight = hbar5Primary.clientHeight;
	  	  
	  // listen for scroll event and call animate function
	  document.addEventListener('scroll', animate);

	  // check if element is in view

	  // check if element is in view
	  function inViewhbar5Primary() {

	    // get window height
	    var windowHeight = window.innerHeight;
	    
	    // get number of pixels that the document is scrolled
	    var scrollY = window.scrollY || window.pageYOffset;

	    // get current scroll position (distance from the top of the page to the bottom of the current viewport)
	    var scrollPosition = scrollY + windowHeight;

	    // get element position (distance from the top of the page to the bottom of the element)
	    var hbar5PrimaryPositionIn = hbar5Primary.getBoundingClientRect().top + scrollY;

	    // get element position (distance from the top of the page to the bottom of the element)
	    var hbar5PrimaryPositionOut = hbar5Primary.getBoundingClientRect().top + scrollY + hbar5PrimaryHeight + windowHeight;    
	    
	    // is scroll position greater than element position? (is element in view?)
	    if ( (scrollPosition > hbar5PrimaryPositionIn) && (scrollPosition < hbar5PrimaryPositionOut) ) {
	      return true;
	    }

	    return false;
	  }
	  
	  // animate element when it is in view
	  function animate() {

	    if (inViewhbar5Primary()) {
	        hbar5Primary.classList.add('hbar'+hbar5PrimaryValue);
	        hbar5Primary.classList.add('hbar-animation');
	    } else {
	        hbar5Primary.classList.remove('hbar'+hbar5PrimaryValue);
	        hbar5Primary.classList.remove('hbar-animation');
	    }

	  }
	    
	});

</script>