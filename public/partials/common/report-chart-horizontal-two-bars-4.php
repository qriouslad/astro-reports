<?php

echo '<div class="report__chart-horizontal-bars">';
	include 'part/report-chart-horizontal-two-bars-xnumbers.php';
	echo '<div id="hbar4primary" class="report__chart-hbar report__chart-hbar--primary"></div>';
	echo '<div id="hbar4secondary" class="report__chart-hbar report__chart-hbar--secondary"></div>';
echo '</div>';

?>

<script type="text/javascript">

	jQuery( document ).ready(function() {
	  
	  const hbar4PrimaryValue = <?php echo $hbar4_primary; ?> * 10;
	  const hbar4SecondaryValue = <?php echo $hbar4_secondary; ?> * 10;
	  
	  var hbar4Primary = document.getElementById('hbar4primary');
	  var hbar4PrimaryHeight = hbar4Primary.clientHeight;
	  
	  var hbar4Secondary = document.getElementById('hbar4secondary');
	  var hbar4SecondaryHeight = hbar4Secondary.clientHeight;
	  
	  // listen for scroll event and call animate function
	  document.addEventListener('scroll', animate);

	  // check if element is in view

	  // check if element is in view
	  function inViewhbar4Primary() {

	    // get window height
	    var windowHeight = window.innerHeight;
	    
	    // get number of pixels that the document is scrolled
	    var scrollY = window.scrollY || window.pageYOffset;

	    // get current scroll position (distance from the top of the page to the bottom of the current viewport)
	    var scrollPosition = scrollY + windowHeight;

	    // get element position (distance from the top of the page to the bottom of the element)
	    var hbar4PrimaryPositionIn = hbar4Primary.getBoundingClientRect().top + scrollY;

	    // get element position (distance from the top of the page to the bottom of the element)
	    var hbar4PrimaryPositionOut = hbar4Primary.getBoundingClientRect().top + scrollY + hbar4PrimaryHeight + windowHeight;    
	    
	    // is scroll position greater than element position? (is element in view?)
	    if ( (scrollPosition > hbar4PrimaryPositionIn) && (scrollPosition < hbar4PrimaryPositionOut) ) {
	      return true;
	    }

	    return false;
	  }

	  // check if element is in view
	  function inViewhbar4Secondary() {

	    // get window height
	    var windowHeight = window.innerHeight;
	    
	    // get number of pixels that the document is scrolled
	    var scrollY = window.scrollY || window.pageYOffset;

	    // get current scroll position (distance from the top of the page to the bottom of the current viewport)
	    var scrollPosition = scrollY + windowHeight;

	    // get element position (distance from the top of the page to the bottom of the element)
	    var hbar4SecondaryPositionIn = hbar4Secondary.getBoundingClientRect().top + scrollY;

	    // get element position (distance from the top of the page to the bottom of the element)
	    var hbar4SecondaryPositionOut = hbar4Secondary.getBoundingClientRect().top + scrollY + hbar4SecondaryHeight + windowHeight;    
	    
	    // is scroll position greater than element position? (is element in view?)
	    if ( (scrollPosition > hbar4SecondaryPositionIn) && (scrollPosition < hbar4SecondaryPositionOut) ) {
	      return true;
	    }

	    return false;
	  }
	  
	  // animate element when it is in view
	  function animate() {

	    if (inViewhbar4Primary()) {
	        hbar4Primary.classList.add('hbar'+hbar4PrimaryValue);
	        hbar4Primary.classList.add('hbar-animation');
	    } else {
	        hbar4Primary.classList.remove('hbar'+hbar4PrimaryValue);
	        hbar4Primary.classList.remove('hbar-animation');
	    }
	                         
	    if (inViewhbar4Secondary()) {
	        hbar4Secondary.classList.add('hbar'+hbar4SecondaryValue);
	        hbar4Secondary.classList.add('hbar-animation');
	    } else {
	        hbar4Secondary.classList.remove('hbar'+hbar4SecondaryValue);
	        hbar4Secondary.classList.remove('hbar-animation');
	    }

	  }
	    
	});

</script>