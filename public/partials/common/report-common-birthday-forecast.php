<?php

echo '<div class="report__article">';

if ( empty($planet1) ) {

	echo '<h4 class="report__article-title">'.$heading.'</h4>';

	include 'report-common-variation-content.php';

} else {

	if ( $aspect == 'in' ) {

		echo '<span class="report__article-subtitle"><img src="/wp-content/uploads/images/glyphs/'.$planet1.'_white.png" /><img src="/wp-content/uploads/images/glyphs/'.$planet2.'_white.png" />'.$interaspect.'</span>';

		echo '<h4 class="report__article-title">'.$heading.'</h4>';

    } elseif ( $aspect == 'retrograde' ) {

		echo '<span class="report__article-subtitle"><img src="/wp-content/uploads/images/glyphs/'.$planet1.'_white.png" /><img src="/wp-content/uploads/images/glyphs/'.$aspect.'_white.png" />'.$interaspect.'</span>';

		echo '<h4 class="report__article-title">'.$heading.'</h4>';

    } else {

		echo '<span class="report__article-subtitle"><img src="/wp-content/uploads/images/glyphs/'.$planet1.'_white.png" /><img src="/wp-content/uploads/images/glyphs/'.$aspect.'_white.png" /><img src="/wp-content/uploads/images/glyphs/'.$planet2.'_white.png" />'.$interaspect.'</span>';

		echo '<h4 class="report__article-title">'.$heading.'</h4>';

    }

	include 'report-common-variation-content.php';

}

echo '</div>';