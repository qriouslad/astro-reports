<?php

echo '<div class="report__chart-vertical-bars">';
	echo '<div class="report__chart-vertical-bars-wrapper">';
		echo '<div class="report__chart-vbar-yaxis"><span>10</span><span>9</span><span>8</span><span>7</span><span>6</span><span>5</span><span>4</span><span>3</span><span>2</span><span>1</span><span>0</span></div>';
		echo '<div class="report__chart-vtwobars-line1"></div>';
		echo '<div class="report__chart-vtwobars-line2"></div>';
		echo '<div id="vbar1" class="report__chart-vbar1"><span class="report__chart-vscore vscore1">'.$vbar1_score.'</span></div>';
		echo '<div id="vbar2" class="report__chart-vbar2"><span class="report__chart-vscore vscore2">'.$vbar2_score.'</span></div>';
	echo '</div>';
echo '</div>';

?>

<script type="text/javascript">

jQuery( document ).ready(function() {
   
  var vbargraph = document.querySelector('.report__chart-vertical-bars');
  var vbargraphHeight = vbargraph.clientHeight;

  var vbar1_score = <?php echo $vbar1_score; ?>;
  var vbar2_score = <?php echo $vbar2_score; ?>;

  if ( vbar1_score == 0 ) {

  	vbar1HpositionAdjustment = 400;

  } else if ( vbar1_score == 1 ) {

  	vbar1HpositionAdjustment = 362;

  } else if ( vbar1_score == 2 ) {

  	vbar1HpositionAdjustment = 324;

  } else if ( vbar1_score == 3 ) {

  	vbar1HpositionAdjustment = 288;

  } else if ( vbar1_score == 4 ) {

  	vbar1HpositionAdjustment = 252;

  } else if ( vbar1_score == 5 ) {

  	vbar1HpositionAdjustment = 216;

  } else if ( vbar1_score == 6 ) {

  	vbar1HpositionAdjustment = 182;

  } else if ( vbar1_score == 7 ) {

  	vbar1HpositionAdjustment = 143;

  } else if ( vbar1_score == 8 ) {

  	vbar1HpositionAdjustment = 107;

  } else if ( vbar1_score == 9 ) {

  	vbar1HpositionAdjustment = 70;

  } else if ( vbar1_score == 10 ) {

  	vbar1HpositionAdjustment = 35;

  } else {}

  if ( vbar2_score == 0 ) {

  	vbar2HpositionAdjustment = 400;

  } else if ( vbar2_score == 1 ) {

  	vbar2HpositionAdjustment = 362;

  } else if ( vbar2_score == 2 ) {

  	vbar2HpositionAdjustment = 324;

  } else if ( vbar2_score == 3 ) {

  	vbar2HpositionAdjustment = 288;

  } else if ( vbar2_score == 4 ) {

  	vbar2HpositionAdjustment = 252;

  } else if ( vbar2_score == 5 ) {

  	vbar2HpositionAdjustment = 216;

  } else if ( vbar2_score == 6 ) {

  	vbar2HpositionAdjustment = 182;

  } else if ( vbar2_score == 7 ) {

  	vbar2HpositionAdjustment = 143;

  } else if ( vbar2_score == 8 ) {

  	vbar2HpositionAdjustment = 107;

  } else if ( vbar2_score == 9 ) {

  	vbar2HpositionAdjustment = 70;

  } else if ( vbar2_score == 10 ) {

  	vbar2HpositionAdjustment = 35;

  } else {}

  const vbar1Hposition = ( 400 - vbar1HpositionAdjustment);
  const vbar2Hposition = ( 400 - vbar2HpositionAdjustment);
  
  // var vbar1 = document.getElementById('vbar1');
  // var vbar2 = document.getElementById('vbar2');
  var vscore1 = document.querySelector('.vscore1');
  var vscore2 = document.querySelector('.vscore2');
  
  // listen for scroll event and call animate function
  document.addEventListener('scroll', animate);

  // check if element is in view
  function inViewVbar() {

    // get window height
    var windowHeight = window.innerHeight;
    
    // get number of pixels that the document is scrolled
    var scrollY = window.scrollY || window.pageYOffset;

    // get current scroll position (distance from the top of the page to the bottom of the current viewport)
    var scrollPosition = scrollY + windowHeight;

    // get element position (distance from the top of the page to the bottom of the element)
    var vbargraphHeightPositionIn = vbargraph.getBoundingClientRect().top + scrollY;

    // get element position (distance from the top of the page to the bottom of the element)
    var vbargraphHeightPositionOut = vbargraph.getBoundingClientRect().top + scrollY + vbargraphHeight + windowHeight;    
    
    // is scroll position greater than element position? (is element in view?)
    if ( (scrollPosition > vbargraphHeightPositionIn) && (scrollPosition < vbargraphHeightPositionOut) ) {

    	return true;

    } else {
	
		return false;

    }

  }
  
  // animate element when it is in view
  function animate() {

    if (inViewVbar()) {
	    jQuery('#vbar1').css("height", vbar1Hposition + "px");
	    jQuery('#vbar2').css("height", vbar2Hposition + "px");
        vscore1.classList.add('report__chart-vbar-animate');
        vscore2.classList.add('report__chart-vbar-animate');
    } else {
	    jQuery('#vbar1').css("height", "0%");
	    jQuery('#vbar2').css("height", "0%");
        vscore1.classList.remove('report__chart-vbar-animate');
        vscore2.classList.remove('report__chart-vbar-animate');
    }
    
  }
    
});

</script>