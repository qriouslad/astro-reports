<?php

// Month Number
if ( !empty( $_POST['atv-birth-month-2'] ) ) {

	$birthmonth2  = htmlspecialchars( $_POST['atv-birth-month-2'] );

} else {


	if ( isset( $_GET['DOB'] ) ) {

		$id = base64_decode( $_GET['DOB'] );

	} else {

		$id2  = htmlspecialchars( $_GET['ID2'] );

		$id2 = base64_decode( $id2 );  

	}

	$datepart2 = explode("/", $id2);
	$birthmonth2 = $datepart2[1];

}

// Day Number
if ( !empty( $_POST['atv-birth-day-2'] ) ) {

	$birthday2  = htmlspecialchars( $_POST['atv-birth-day-2'] );

} else {

	if ( isset($_GET['DOB']) ) {

		$id = base64_decode( $_GET['DOB'] );

	} else {

		$id2  = htmlspecialchars( $_GET['ID2'] );

		$id2 = base64_decode( $id2 );  

	}

	$datepart2 = explode("/", $id2);
	$birthday2 = $datepart2[2];

}

// Year Number
if ( !empty( $_POST['atv-birth-year-2'] ) ) {

	$birthyear2  = htmlspecialchars( $_POST['atv-birth-year-2'] );

	$id2 = 'defined';

} else {

	if ( isset( $_GET['DOB'] ) ) {

		$id = base64_decode( $_GET['DOB'] );

	} else {

		$id2  = htmlspecialchars($_GET['ID2']);

		$id2 = base64_decode( $id2 );  

	}

	$datepart2 = explode("/", $id2);
	$birthyear2 = $datepart2[0];

}

$dateofbirth2 = $birthyear2.'/'.$birthmonth2.'/'.$birthday2;

if ( $dateofbirth2 == '//' ) {

	$id2 = 'defined';

	$dateofbirth2 = '1985/12/31';

} else {}

$dateofbirth2_formatted = date("F j, Y", strtotime($dateofbirth2) );

$datetime2 = date("n/d/Y", strtotime( $dateofbirth2 ) );
$datetimePlain2 = date("ndY", strtotime( $dateofbirth2 ) );
$dateformatted2 = date("F jS, Y", strtotime( $dateofbirth2 ) );