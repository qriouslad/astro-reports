<?php

// Month Number
if ( !empty( $_POST['atv-birth-month'] ) ) {

	$birthmonth  = htmlspecialchars( $_POST['atv-birth-month'] );

} else {


	if ( isset( $_GET['DOB'] ) ) {

		$id = base64_decode( $_GET['DOB'] );

	} else {

		$id  = htmlspecialchars( $_GET['ID'] );

		$id = base64_decode( $id );  

	}

	$datepart = explode("/", $id);
	$birthmonth = $datepart[1];

}

// Day Number
if ( !empty( $_POST['atv-birth-day'] ) ) {

	$birthday  = htmlspecialchars( $_POST['atv-birth-day'] );

} else {

	if ( isset($_GET['DOB']) ) {

		$id = base64_decode( $_GET['DOB'] );

	} else {

		$id  = htmlspecialchars( $_GET['ID'] );

		$id = base64_decode( $id );  

	}

	$datepart = explode("/", $id);
	$birthday = $datepart[2];

}

// Year Number
if ( !empty( $_POST['atv-birth-year'] ) ) {

	$birthyear  = htmlspecialchars( $_POST['atv-birth-year'] );

	$id = 'defined';

} else {

	if ( isset( $_GET['DOB'] ) ) {

		$id = base64_decode( $_GET['DOB'] );

	} else {

		$id  = htmlspecialchars($_GET['ID']);

		$id = base64_decode( $id );  

	}

	$datepart = explode("/", $id);
	$birthyear = $datepart[0];

}

$dateofbirth = $birthyear.'/'.$birthmonth.'/'.$birthday;

if ( $dateofbirth == '//' ) {

	$id = 'defined';

	$dateofbirth = '1980/01/09';

} else {}


$dateofbirth_formatted = date("F j, Y", strtotime($dateofbirth) );

$datetime = date("n/d/Y", strtotime( $dateofbirth ) );
$datetimePlain = date("ndY", strtotime( $dateofbirth ) );
$dateformatted = date("F jS, Y", strtotime( $dateofbirth ) );