<?php

// First name of first person
if ( !empty( $_POST['atv-first-name'] ) ) {

	$fn = htmlspecialchars( $_POST['atv-first-name'] );

} elseif ( isset( $_GET['FN'] ) ) {

	$fn = urlencode( htmlspecialchars( $_GET['FN'] ) );

} else {

	$fn = 'John';

}

// First name of second person
if ( !empty( $_POST['atv-first-name-2'] ) ) {

	$fn2 = htmlspecialchars( $_POST['atv-first-name-2'] );

} elseif ( isset( $_GET['FN2'] ) ) {

	$fn2 = urlencode( htmlspecialchars( $_GET['FN2'] ) );

} else {

	$fn2 = 'Jane';

}

if ( !empty( $_POST['atv-email'] ) ) {

	$email  = htmlspecialchars( $_POST['atv-email'] );

} else {

	$email = urlencode( htmlspecialchars( $_GET['E'] ) );

}

// $updates  = htmlspecialchars($_POST['UPDATES']);