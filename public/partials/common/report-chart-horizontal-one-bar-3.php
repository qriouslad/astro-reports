<?php

echo '<div class="report__chart-horizontal-bars report__chart-horizontal-bars--one-bar">';
	include 'part/report-chart-horizontal-two-bars-xnumbers.php';
	echo '<div id="hbar3primary" class="report__chart-hbar report__chart-hbar--primary"></div>';
echo '</div>';

?>

<script type="text/javascript">

	jQuery( document ).ready(function() {
	  
	  const hbar3PrimaryValue = <?php echo $hbar3_primary; ?> * 10;
	  
	  var hbar3Primary = document.getElementById('hbar3primary');
	  var hbar3PrimaryHeight = hbar3Primary.clientHeight;
	  	  
	  // listen for scroll event and call animate function
	  document.addEventListener('scroll', animate);

	  // check if element is in view

	  // check if element is in view
	  function inViewhbar3Primary() {

	    // get window height
	    var windowHeight = window.innerHeight;
	    
	    // get number of pixels that the document is scrolled
	    var scrollY = window.scrollY || window.pageYOffset;

	    // get current scroll position (distance from the top of the page to the bottom of the current viewport)
	    var scrollPosition = scrollY + windowHeight;

	    // get element position (distance from the top of the page to the bottom of the element)
	    var hbar3PrimaryPositionIn = hbar3Primary.getBoundingClientRect().top + scrollY;

	    // get element position (distance from the top of the page to the bottom of the element)
	    var hbar3PrimaryPositionOut = hbar3Primary.getBoundingClientRect().top + scrollY + hbar3PrimaryHeight + windowHeight;    
	    
	    // is scroll position greater than element position? (is element in view?)
	    if ( (scrollPosition > hbar3PrimaryPositionIn) && (scrollPosition < hbar3PrimaryPositionOut) ) {
	      return true;
	    }

	    return false;
	  }
	  
	  // animate element when it is in view
	  function animate() {

	    if (inViewhbar3Primary()) {
	        hbar3Primary.classList.add('hbar'+hbar3PrimaryValue);
	        hbar3Primary.classList.add('hbar-animation');
	    } else {
	        hbar3Primary.classList.remove('hbar'+hbar3PrimaryValue);
	        hbar3Primary.classList.remove('hbar-animation');
	    }

	  }
	    
	});

</script>