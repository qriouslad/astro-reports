<?php

if ( $atts['variation'] == 'short' ) {

	echo '<p class="report__article-content report__article-content--short"><img class="report__article-image report__article-short-image" src="/wp-content/uploads/images/reports/'.$random_image_name.'.jpg" />';

} elseif ( $atts['variation'] == 'full' ) {

	echo '<p class="report__article-content report__article-content--full"><img class="report__article-image report__article-full-image" src="/wp-content/uploads/images/reports/'.$random_image_name.'.jpg" />';

} else {}

echo '<span>'.$text;

if ( $report_version == 'free' ) {

	echo '<br /><a class="report__article-link" href="#purchase">Get my full report</a>';

} else {}

echo '</span></p>';