<?php

echo '<div class="report__chart-horizontal-bars">';
	include 'part/report-chart-horizontal-two-bars-xnumbers.php';
	echo '<div id="hbar1primary" class="report__chart-hbar report__chart-hbar--primary"></div>';
	echo '<div id="hbar1secondary" class="report__chart-hbar report__chart-hbar--secondary"></div>';
echo '</div>';

?>

<script type="text/javascript">

	jQuery( document ).ready(function() {
	  
	  const hbar1PrimaryValue = <?php echo $hbar1_primary; ?> * 10;
	  const hbar1SecondaryValue = <?php echo $hbar1_secondary; ?> * 10;
	  
	  var hbar1Primary = document.getElementById('hbar1primary');
	  var hbar1PrimaryHeight = hbar1Primary.clientHeight;
	  
	  var hbar1Secondary = document.getElementById('hbar1secondary');
	  var hbar1SecondaryHeight = hbar1Secondary.clientHeight;
	  
	  // listen for scroll event and call animate function
	  document.addEventListener('scroll', animate);

	  // check if element is in view

	  // check if element is in view
	  function inViewhbar1Primary() {

	    // get window height
	    var windowHeight = window.innerHeight;
	    
	    // get number of pixels that the document is scrolled
	    var scrollY = window.scrollY || window.pageYOffset;

	    // get current scroll position (distance from the top of the page to the bottom of the current viewport)
	    var scrollPosition = scrollY + windowHeight;

	    // get element position (distance from the top of the page to the bottom of the element)
	    var hbar1PrimaryPositionIn = hbar1Primary.getBoundingClientRect().top + scrollY;

	    // get element position (distance from the top of the page to the bottom of the element)
	    var hbar1PrimaryPositionOut = hbar1Primary.getBoundingClientRect().top + scrollY + hbar1PrimaryHeight + windowHeight;    
	    
	    // is scroll position greater than element position? (is element in view?)
	    if ( (scrollPosition > hbar1PrimaryPositionIn) && (scrollPosition < hbar1PrimaryPositionOut) ) {
	      return true;
	    }

	    return false;
	  }

	  // check if element is in view
	  function inViewhbar1Secondary() {

	    // get window height
	    var windowHeight = window.innerHeight;
	    
	    // get number of pixels that the document is scrolled
	    var scrollY = window.scrollY || window.pageYOffset;

	    // get current scroll position (distance from the top of the page to the bottom of the current viewport)
	    var scrollPosition = scrollY + windowHeight;

	    // get element position (distance from the top of the page to the bottom of the element)
	    var hbar1SecondaryPositionIn = hbar1Secondary.getBoundingClientRect().top + scrollY;

	    // get element position (distance from the top of the page to the bottom of the element)
	    var hbar1SecondaryPositionOut = hbar1Secondary.getBoundingClientRect().top + scrollY + hbar1SecondaryHeight + windowHeight;    
	    
	    // is scroll position greater than element position? (is element in view?)
	    if ( (scrollPosition > hbar1SecondaryPositionIn) && (scrollPosition < hbar1SecondaryPositionOut) ) {
	      return true;
	    }

	    return false;
	  }
	  
	  // animate element when it is in view
	  function animate() {

	    if (inViewhbar1Primary()) {
	        hbar1Primary.classList.add('hbar'+hbar1PrimaryValue);
	        hbar1Primary.classList.add('hbar-animation');
	    } else {
	        hbar1Primary.classList.remove('hbar'+hbar1PrimaryValue);
	        hbar1Primary.classList.remove('hbar-animation');
	    }
	                         
	    if (inViewhbar1Secondary()) {
	        hbar1Secondary.classList.add('hbar'+hbar1SecondaryValue);
	        hbar1Secondary.classList.add('hbar-animation');
	    } else {
	        hbar1Secondary.classList.remove('hbar'+hbar1SecondaryValue);
	        hbar1Secondary.classList.remove('hbar-animation');
	    }

	  }
	    
	});

</script>