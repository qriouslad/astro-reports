<?php

$image_array = ['general','romantic'];
$random_image_pool = $image_array[mt_rand(0, count($image_array) - 1)];

$general_images_total = 173;
$general_image_num = mt_rand(1, $general_images_total);
$romantic_images_total = 152;
$romantic_image_num = mt_rand(1, $romantic_images_total);

if ( $random_image_pool == 'general' ) {
  $random_image_name = $random_image_pool.''.$general_image_num;
} elseif ( $random_image_pool == 'romantic' ) {
  $random_image_name = $random_image_pool.''.$romantic_image_num;
} else {}