<?php

echo '<div class="report__chart-horizontal-bars report__chart-horizontal-bars--one-bar">';
	include 'part/report-chart-horizontal-two-bars-xnumbers.php';
	echo '<div id="hbar2primary" class="report__chart-hbar report__chart-hbar--primary"></div>';
echo '</div>';

?>

<script type="text/javascript">

	jQuery( document ).ready(function() {
	  
	  const hbar2PrimaryValue = <?php echo $hbar2_primary; ?> * 10;
	  
	  var hbar2Primary = document.getElementById('hbar2primary');
	  var hbar2PrimaryHeight = hbar2Primary.clientHeight;
	  	  
	  // listen for scroll event and call animate function
	  document.addEventListener('scroll', animate);

	  // check if element is in view

	  // check if element is in view
	  function inViewhbar2Primary() {

	    // get window height
	    var windowHeight = window.innerHeight;
	    
	    // get number of pixels that the document is scrolled
	    var scrollY = window.scrollY || window.pageYOffset;

	    // get current scroll position (distance from the top of the page to the bottom of the current viewport)
	    var scrollPosition = scrollY + windowHeight;

	    // get element position (distance from the top of the page to the bottom of the element)
	    var hbar2PrimaryPositionIn = hbar2Primary.getBoundingClientRect().top + scrollY;

	    // get element position (distance from the top of the page to the bottom of the element)
	    var hbar2PrimaryPositionOut = hbar2Primary.getBoundingClientRect().top + scrollY + hbar2PrimaryHeight + windowHeight;    
	    
	    // is scroll position greater than element position? (is element in view?)
	    if ( (scrollPosition > hbar2PrimaryPositionIn) && (scrollPosition < hbar2PrimaryPositionOut) ) {
	      return true;
	    }

	    return false;
	  }
	  
	  // animate element when it is in view
	  function animate() {

	    if (inViewhbar2Primary()) {
	        hbar2Primary.classList.add('hbar'+hbar2PrimaryValue);
	        hbar2Primary.classList.add('hbar-animation');
	    } else {
	        hbar2Primary.classList.remove('hbar'+hbar2PrimaryValue);
	        hbar2Primary.classList.remove('hbar-animation');
	    }

	  }
	    
	});

</script>