<?php

$report_name = 'love-hurts';

include 'common/report-common-two-person.php';

if ( empty ( $fn ) || empty ( $id ) || empty ( $fn2 ) || empty ( $id2 ) ) {

	include 'common/report-not-found.php';

} else {

	include 'common/report-common-site-date-info.php';

	include 'common/report-common-variation.php';

	$love_hurts_data_cachekey = 'lh_'.$atts['variation'].'_'.$id.'_'.$id2;

	$love_hurts_data_cached = simplexml_load_string( get_transient( $love_hurts_data_cachekey ) );

	if ( false === $love_hurts_data_cached ) {

		$url = "https://www.zdki.us/taReportsw/MakeReport.aspx?ReportID=10&ReportVariation=".$report_variation."&ReportFormat=XML&Name1=".$fn."&BirthDate1=".$datetime."&Name2=".$fn2."&BirthDate2=".$datetime2."&StartDate=&Length=&AccountID=".$accountid."&AppID=astroreports_NUM&MemberID=numerologist1&V=2";

		// defines $data variable with API's response
		include 'common/report-common-http-auth.php';

		$report_content_raw = simplexml_load_string($data);

		set_transient( $love_hurts_data_cachekey, $report_content_raw->asXML(), MONTH_IN_SECONDS );

		// echo '<h6>Uncached data is used</h6>';

	} else {

		$report_content_raw = $love_hurts_data_cached;

		// echo '<h6>Cached data is used. Cache key: '.$love_hurts_data_cachekey.'</h6>';

	}

	$friction = $report_content_raw->summary[0]->squareindex;
	$differences = $report_content_raw->summary[0]->oppositionindex;
	$karma = $report_content_raw->summary[0]->quincunxindex;

	$vbar1_score = $friction;
	$vbar2_score = $differences;
	$vbar3_score = $karma;

	// $friction_padded = sprintf("%02d", $friction);
	// $differences_padded = sprintf("%02d", $differences);
	// $karma_padded = sprintf("%02d", $karma);

	// echo '<h4 class="report__subtitle">Prepared for '.urldecode( $fn ).', born on '.$dateformatted.', and '.urldecode( $fn2 ).', born on '.$dateformatted2.'</h4>';

	// 'free' or 'full'
	include 'common/report-common-report-version.php';

	// ============ Report Charts ============

	if ( $report_part == 'charts' ) {

		if ( $report_version == 'full' ) {


			echo '<div class="report__chart-section">';
				echo '<h3 class="report__chart-title"><span>Friction, Differences, and Karma Meters</span></h2>';
				echo '<div class="report__chart-section-wrapper">';

					echo '<div class="report__chart-description">';
						echo '<div class="report__chart-description-wrapper">';
							echo "<p>The Friction, Differences and Karma Meters: every couple faces a certain set of challenges, and the number and type of challenges determine whether your relationship will be merely interesting or out-and-out difficult. At The Astrologer, we've broken down these challenges into three sections: sources of friction, the major differences between you, and the karmic ties from past lives that draw you together today.</p><p>On the first two meters, Friction and Differences, a score somewhere in the middle is probably best. Friction can be a good thing -- after all, the same energy that makes you lust after each other also makes you fight. And we all know that a good argument, like good sex, clears the air and lets you know your passion is alive! The same goes for differences between you in the ways that you think and view the world: These contrasts keep the relationship dynamic.</p><p>Karma, on the other hand, isn't such a great thing, even if your past-life bonds were positive ones. This lifetime is for moving forward, not for reliving an old relationship from a past one. A low score on the Karma Meter is best as it means you can enjoy each other without working through too much karma.</p>";
						echo '</div>';
					echo '</div>';
					echo '<div class="report__chart-visuals">';
						echo '<div class="report__chart-animation">';
							include 'common/report-chart-vertical-three-bars.php';
						echo '</div>';
						echo '<div class="report__chart-legends report__chart-legends--thirds">';

							echo '<div class="report__chart-legends-third-left">';
							    echo '<h4 class="report__chart-legends-title report__chart-legends-title--third report__chart-legends-title-third-left">Friction = '.$friction.'</h4>';
								echo '<p><span class="report__chart-legends-score">7-10</span>: Open warfare</p>';
								echo '<p><span class="report__chart-legends-score">4-6</span>: The occasional dispute</p>';
								echo '<p><span class="report__chart-legends-score">1-3</span>: Whatever you have to say dear</p>';
							echo '</div>';

							echo '<div class="report__chart-legends-third-center">';
							    echo '<h4 class="report__chart-legends-title report__chart-legends-title--third report__chart-legends-title-third-center">Differences = '.$differences.'</h4>';
								echo '<p><span class="report__chart-legends-score">7-10</span>: Who the $#@% are you, again?</p>';
								echo '<p><span class="report__chart-legends-score">4-6</span>: Some interesting contrasts</p>';
								echo '<p><span class="report__chart-legends-score">1-3</span>: Two Peas in a pod</p>';
							echo '</div>';

							echo '<div class="report__chart-legends-third-right">';
							    echo '<h4 class="report__chart-legends-title report__chart-legends-title--third report__chart-legends-title-third-right">Karma = '.$karma.'</h4>';
								echo '<p><span class="report__chart-legends-score">7-10</span>: New love, same old story</p>';
								echo '<p><span class="report__chart-legends-score">4-6</span>: Total deja-vu</p>';
								echo '<p><span class="report__chart-legends-score">1-3</span>: No past life ties (whew!)</p>';
							echo '</div>';

						echo '</div>';
					echo '</div>';

				echo '</div>';
			echo '</div>';

		} else {

			include 'common/report-common-charts-preview.php';

		}

	} else {}

	// ============ Report Articles ============

	if ( $report_part == 'articles' ) {

		include 'common/report-free-limits-start.php';

		foreach ( $report_content_raw->detail[0]->section as $section ) {

			if ( $report_version == 'free' ) {

				if ( $i == $limit ) break;

			} else {}

			echo '<div class="report__section">';
			echo '<h2 class="report__section-title"><span>'.$section->title.'</span></h2>';

			foreach ( $section->paragraph as $paragraph ) {

				include 'common/report-common-content-interaspect.php';

				$heading_lower = strtolower($heading);

				include 'common/report-image-pool-complete.php';

				echo '<div class="report__article">';

				if ( $planet1 == 'north' ) {

					$planet1 = 'northnode';
					$aspect = strtolower($interaspctchunk[2]);
					$planet2 = strtolower($interaspctchunk[3]);

				}

				if ( empty($planet1) ) {

					if ( $heading_lower == 'few challenges equals little growth' ) {

						$interaspect = 'No squares';
						$planet1 = 'square';
						// $interaspect_slug = 'no-squares';

					} elseif ( $heading_lower == 'finding a wider perspective' ) {

						$interaspect = 'No oppositions';
						$planet1 = 'opposite';
						// $interaspect_slug = 'no-oppositions';

					} elseif ( $heading_lower == 'together because you want to be' ) {

						$interaspect = 'No quincunxes';
						$planet1 = 'quincunx';
						// $interaspect_slug = 'no-quincunxes';

					} elseif ( $heading_lower == "enjoying togetherness and apartness" ) {

						$interaspect = 'No conjunctions';
						$planet1 = 'conjunct';
						// $interaspect_slug = 'no-conjunctions';

					}

					echo '<span class="report__article-subtitle"><img src="/wp-content/uploads/images/glyphs/unaspected_white.png" /><img src="/wp-content/uploads/images/glyphs/'.$planet1.'_white.png" />'.$interaspect.'</span>';

					echo '<h4 class="report__article-title">'.$heading.'</h4>';

					include 'common/report-common-variation-content.php';

				} else {

					if ( $aspect == 'in' ) {

						echo '<span class="report__article-subtitle"><img src="/wp-content/uploads/images/glyphs/'.$planet1.'_white.png" /><img src="/wp-content/uploads/images/glyphs/'.$planet2.'_white.png" />'.$interaspect.'</span>';

						echo '<h4 class="report__article-title">'.$heading.'</h4>';

					}  else {

						echo '<span class="report__article-subtitle"><img src="/wp-content/uploads/images/glyphs/'.$planet1.'_white.png" /><img src="/wp-content/uploads/images/glyphs/'.$aspect.'_white.png" /><img src="/wp-content/uploads/images/glyphs/'.$planet2.'_white.png" />'.$interaspect.'</span>';

						echo '<h4 class="report__article-title">'.$heading.'</h4>';

					}

					include 'common/report-common-variation-content.php';
				    
				}

			    echo '</div>';

			}

		    echo '</div>';

			include 'common/report-free-limits-end.php';

		}

	} else {}

}