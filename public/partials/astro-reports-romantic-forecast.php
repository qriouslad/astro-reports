<?php

$report_name = 'romantic-forecast';

include 'common/report-common-one-person.php';

if ( empty ( $fn ) || empty ( $id ) || empty ( $sd ) ) {

	include 'common/report-not-found.php';

} else {

	include 'common/report-common-site-date-info.php';

	include 'common/report-common-variation.php';

	$romantic_forecast_data_cachekey = 'rf_'.$atts['variation'].'_'.$id.'_'.$datetimeStringPlain;

	$romantic_forecast_data_cached = simplexml_load_string( get_transient( $romantic_forecast_data_cachekey ) );

	if ( false === $romantic_forecast_data_cached ) {

		$url = "https://www.zdki.us/taReportsw/MakeReport.aspx?ReportID=7&ReportVariation=".$report_variation."&ReportFormat=XML&Name1=".$fn."&BirthDate1=".$datetime."&Name2=&BirthDate2=&StartDate=".$datetimeString."&Length=1&AccountID=".$accountid."&AppID=astroreports_NUM&MemberID=numerologist1&V=2";

		// defines $data variable with API's response
		include 'common/report-common-http-auth.php';

		$report_content_raw = simplexml_load_string($data);

		set_transient( $romantic_forecast_data_cachekey, $report_content_raw->asXML(), MONTH_IN_SECONDS );

		// echo '<h6>Uncached data is used</h6>';

	} else {

		$report_content_raw = $romantic_forecast_data_cached;

		// echo '<h6>Cached data is used. Cache key: '.$romantic_forecast_data_cachekey.'</h6>';

	}

	$summary_index1 = $report_content_raw->summary[0]->chaptersum[0]->index;
	$summary_index2 = $report_content_raw->summary[0]->chaptersum[1]->index;
	$summary_index3 = $report_content_raw->summary[0]->chaptersum[2]->index;
	$summary_index4 = $report_content_raw->summary[0]->chaptersum[3]->index;
	$summary_index5 = $report_content_raw->summary[0]->chaptersum[4]->index;
	$summary_index6 = $report_content_raw->summary[0]->chaptersum[5]->index;

	$hbar1_primary = $summary_index1;
	$hbar2_primary = $summary_index2;
	$hbar3_primary = $summary_index3;
	$hbar4_primary = $summary_index4;
	$hbar5_primary = $summary_index5;
	$hbar6_primary = $summary_index6;

	// echo '<h4 class="report__subtitle">Prepared for '.urldecode( $fn ).', born on '.$dateformatted.'</h4>';

	// 'free' or 'full'
	include 'common/report-common-report-version.php';

	// ============ Report Charts ============

	if ( $report_part == 'charts' ) {

		if ( $report_version == 'full' ) {

			echo '<div class="report__chart-section">';
				echo '<div class="report__chart-description report__chart-description--full-width">';
					echo '<h3 class="report__chart-title"><span>Some Title Here</span></h3>';
					echo '<div class="report__chart-description-wrapper">';
						echo "<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus.</p>";
					echo '</div>';
				echo '</div>';

			echo '</div>';

			echo '<div id="#sun" class="report__chart-section">';
				echo '<h3 class="report__chart-title"><span>Sun - Recognition</span></h3>';
				echo '<div class="report__chart-section-wrapper">';
					echo '<div class="report__chart-description">';
						echo '<div class="report__chart-description-wrapper">';
							echo "<p class=\"report__read-more\">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus.</p>";
						echo '</div>';
					echo '</div>';
					echo '<div class="report__chart-visuals">';
						echo '<div class="report__chart-animation">';
							include 'common/report-chart-horizontal-one-bar-1.php';
						echo '</div>';
						echo '<div class="report__chart-legends">';
							include 'common/report-chart-horizontal-one-bar-legends.php';
						echo '</div>';
					echo '</div>';
				echo '</div>';
			echo '</div>';

			echo '<div id="#mercury" class="report__chart-section">';
				echo '<h3 class="report__chart-title">Mercury - Learning</h3>';
				echo '<div class="report__chart-section-wrapper">';
					echo '<div class="report__chart-description">';
						echo '<div class="report__chart-description-wrapper">';
							echo "<p class=\"report__read-more\">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus.</p>";
						echo '</div>';
					echo '</div>';
					echo '<div class="report__chart-visuals">';
						echo '<div class="report__chart-animation">';
							include 'common/report-chart-horizontal-one-bar-2.php';
						echo '</div>';
						echo '<div class="report__chart-legends">';
							include 'common/report-chart-horizontal-one-bar-legends.php';
						echo '</div>';
					echo '</div>';
				echo '</div>';
			echo '</div>';

			echo '<div id="#venus" class="report__chart-section">';
				echo '<h3 class="report__chart-title">Venus - Romance</h3>';
				echo '<div class="report__chart-section-wrapper">';
					echo '<div class="report__chart-description">';
						echo '<div class="report__chart-description-wrapper">';
							echo "<p class=\"report__read-more\">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus.</p>";
						echo '</div>';
					echo '</div>';
					echo '<div class="report__chart-visuals">';
						echo '<div class="report__chart-animation">';
							include 'common/report-chart-horizontal-one-bar-3.php';
						echo '</div>';
						echo '<div class="report__chart-legends">';
							include 'common/report-chart-horizontal-one-bar-legends.php';
						echo '</div>';
					echo '</div>';
				echo '</div>';
			echo '</div>';

			echo '<div id="#mars" class="report__chart-section">';
				echo '<h3 class="report__chart-title">Mars - Ambition</h3>';
				echo '<div class="report__chart-section-wrapper">';
					echo '<div class="report__chart-description">';
						echo '<div class="report__chart-description-wrapper">';
							echo "<p class=\"report__read-more\">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus.</p>";
						echo '</div>';
					echo '</div>';
					echo '<div class="report__chart-visuals">';
						echo '<div class="report__chart-animation">';
							include 'common/report-chart-horizontal-one-bar-4.php';
						echo '</div>';
						echo '<div class="report__chart-legends">';
							include 'common/report-chart-horizontal-one-bar-legends.php';
						echo '</div>';
					echo '</div>';
				echo '</div>';
			echo '</div>';

			echo '<div id="#jupiter" class="report__chart-section">';
				echo '<h3 class="report__chart-title">Jupiter - Travel</h3>';
				echo '<div class="report__chart-section-wrapper">';
					echo '<div class="report__chart-description">';
						echo '<div class="report__chart-description-wrapper">';
							echo "<p class=\"report__read-more\">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus.</p>";
						echo '</div>';
					echo '</div>';
					echo '<div class="report__chart-visuals">';
						echo '<div class="report__chart-animation">';
							include 'common/report-chart-horizontal-one-bar-5.php';
						echo '</div>';
						echo '<div class="report__chart-legends">';
							include 'common/report-chart-horizontal-one-bar-legends.php';
						echo '</div>';
					echo '</div>';
				echo '</div>';
			echo '</div>';

			echo '<div id="#saturn" class="report__chart-section">';
				echo '<h3 class="report__chart-title">Saturn - Responsibilities</h3>';
				echo '<div class="report__chart-section-wrapper">';
					echo '<div class="report__chart-description">';
						echo '<div class="report__chart-description-wrapper">';
							echo "<p class=\"report__read-more\">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus.</p>";
						echo '</div>';
					echo '</div>';
					echo '<div class="report__chart-visuals">';
						echo '<div class="report__chart-animation">';
							include 'common/report-chart-horizontal-one-bar-6.php';
						echo '</div>';
						echo '<div class="report__chart-legends">';
							include 'common/report-chart-horizontal-one-bar-legends.php';
						echo '</div>';
					echo '</div>';
				echo '</div>';
			echo '</div>';

			include 'common/report-common-read-more-less-js.php';

		} else {

			include 'common/report-common-charts-preview.php';

		}

	} else {}

	// ============ Report Articles ============

	if ( $report_part == 'articles' ) {

		if ( ( ! empty( $report_content_raw->months[0]->month[0]->strengths[0] ) ) || ( ! empty( $report_content_raw->months[0]->month[1]->strengths[0] ) ) ) {

			echo '<div class="report__section">';
			echo '<h2 class="report__section-title"><span>Your Strengths</span></h2>';

		}

		include 'common/report-free-limits-start.php';

		foreach ( $report_content_raw->months[0]->month as $month ) {

			if ( ! empty( $month->strengths[0] ) ) {

				foreach ( $month->strengths[0]->paragraph as $paragraph ) {

					if ( $report_version == 'free' ) {

						if ( $i == 7 ) break;

					} else {}

					if ( $previous_heading != (string)$paragraph->heading ) {

						include 'common/report-common-content-interaspect.php';

						include 'common/report-image-pool-complete.php';

						echo '<div class="report__article">';

						include 'common/report-common-content-heading.php';

						include 'common/report-common-transit-dates.php';

						include 'common/report-common-variation-content.php';

						echo '</div>';

					} else {}

					$previous_heading = (string)$paragraph->heading;

					include 'common/report-free-limits-end.php';

				}

			}

		}

		include 'common/report-free-limits-end.php';

		if ( ( ! empty( $report_content_raw->months[0]->month[0]->strengths[0] ) ) || ( ! empty( $report_content_raw->months[0]->month[1]->strengths[0] ) ) ) {

			echo '</div>';

		}

		if ( $report_version !== 'free' ) {

			if ( ( ! empty( $report_content_raw->months[0]->month[0]->intensities[0] ) )  || ( ! empty( $report_content_raw->months[0]->month[1]->intensities[0] ) ) ) {

				echo '<div class="report__section">';
				echo '<h2 class="report__section-title"><span>Your Intensities</span></h2>'; 

			}

			foreach ( $report_content_raw->months[0]->month as $month ) {

				if ( ! empty( $month->intensities[0] ) ) {

					foreach ( $month->intensities[0]->paragraph as $paragraph ) {

						if ( $previous_heading != (string)$paragraph->heading ) {

							include 'common/report-common-content-interaspect.php';

							include 'common/report-image-pool-complete.php';

							echo '<div class="report__article">';

							include 'common/report-common-content-heading.php';

							include 'common/report-common-transit-dates.php';

							include 'common/report-common-variation-content.php';

							echo '</div>';

						} else {}

						$previous_heading = (string)$paragraph->heading;

					}

				}

			}

			if ( ( ! empty( $report_content_raw->months[0]->month[0]->intensities[0] ) )  || ( ! empty( $report_content_raw->months[0]->month[1]->intensities[0] ) ) ) {

				echo '</div>';

			}

		} else {}

		if ( $report_version !== 'free' ) {

			if ( ( ! empty($report_content_raw->months[0]->month[0]->challenges[0]) ) || ( ! empty($report_content_raw->months[0]->month[1]->challenges[0] ) ) ) {

				echo '<div class="report__section">';
				echo '<h2 class="report__section-title"><span>Your Challenges</span></h2>';

			}

			foreach ( $report_content_raw->months[0]->month as $month ) {

				if ( ! empty( $month->challenges[0] ) ) {

					foreach ( $month->challenges[0]->paragraph as $paragraph ) {

						if ( $previous_heading != (string)$paragraph->heading ) {

							include 'common/report-common-content-interaspect.php';

							include 'common/report-image-pool-complete.php';

							echo '<div class="report__article">';

							include 'common/report-common-content-heading.php';

							include 'common/report-common-transit-dates.php';

							include 'common/report-common-variation-content.php';

							echo '</div>';

						} else {}

						$previous_heading = (string)$paragraph->heading;

					}

				}

			}

			if ( ( ! empty($report_content_raw->months[0]->month[0]->challenges[0]) ) || ( ! empty($report_content_raw->months[0]->month[1]->challenges[0] ) ) ) {

				echo '</div>';

			}

		} else {}

	} else {}

}