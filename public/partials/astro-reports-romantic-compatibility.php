<?php

$report_name = 'romantic-compatibility';

include 'common/report-common-two-person.php';

if ( empty ( $fn ) || empty ( $id ) || empty ( $fn2 ) || empty ( $id2 ) ) {

	include 'common/report-not-found.php';

} else {

	include 'common/report-common-site-date-info.php';

	include 'common/report-common-variation.php';

	$romantic_compatibility_data_cachekey = 'rc_'.$atts['variation'].'_'.$id.'_'.$id2;

	$romantic_compatibility_data_cached = simplexml_load_string( get_transient( $romantic_compatibility_data_cachekey ) );

	if ( false === $romantic_compatibility_data_cached ) {

		$url = "https://www.zdki.us/taReportsw/MakeReport.aspx?ReportID=46&ReportVariation=".$report_variation."&ReportFormat=XML&Name1=".$fn."&BirthDate1=".$datetime."&Name2=".$fn2."&BirthDate2=".$datetime2."&StartDate=&Length=&AccountID=".$accountid."&AppID=astroreports_NUM&MemberID=numerologist1&V=2";

		// defines $data variable with API's response
		include 'common/report-common-http-auth.php';

		$report_content_raw = simplexml_load_string($data);

		set_transient( $romantic_compatibility_data_cachekey, $report_content_raw->asXML(), MONTH_IN_SECONDS );

		// echo '<h6>Uncached data is used</h6>';

	} else {

		$report_content_raw = $romantic_compatibility_data_cached;

		// echo '<h6>Cached data is used. Cache key: '.$romantic_compatibility_data_cachekey.'</h6>';

	}

	$harmony = $report_content_raw->summary[0]->harmonyindex;
	$excitement = $report_content_raw->summary[0]->tensionindex;

	$vbar1_score = $harmony;
	$vbar2_score = $excitement;

	// $harmony_padded = sprintf("%02d", $harmony);
	// $excitement_padded = sprintf("%02d", $excitement);

	$summary_index1 = $report_content_raw->summary[0]->chaptersum[0]->index;
	$summary_index2 = $report_content_raw->summary[0]->chaptersum[1]->index;
	$summary_index3 = $report_content_raw->summary[0]->chaptersum[2]->index;
	$summary_index4 = $report_content_raw->summary[0]->chaptersum[3]->index;
	$summary_index5 = $report_content_raw->summary[0]->chaptersum[4]->index;
	$summary_index6 = $report_content_raw->summary[0]->chaptersum[5]->index;

	$reverse_summary_index1 = $report_content_raw->reverse_summary[0]->chaptersum[0]->index;
	$reverse_summary_index2 = $report_content_raw->reverse_summary[0]->chaptersum[1]->index;
	$reverse_summary_index3 = $report_content_raw->reverse_summary[0]->chaptersum[2]->index;
	$reverse_summary_index4 = $report_content_raw->reverse_summary[0]->chaptersum[3]->index;
	$reverse_summary_index5 = $report_content_raw->reverse_summary[0]->chaptersum[4]->index;
	$reverse_summary_index6 = $report_content_raw->reverse_summary[0]->chaptersum[5]->index;

	$hbar1_primary = $summary_index1;
	$hbar1_secondary = $reverse_summary_index1;

	$hbar2_primary = $summary_index2;
	$hbar2_secondary = $reverse_summary_index2;

	$hbar3_primary = $summary_index3;
	$hbar3_secondary = $reverse_summary_index3;

	$hbar4_primary = $summary_index4;
	$hbar4_secondary = $reverse_summary_index4;

	$hbar5_primary = $summary_index5;
	$hbar5_secondary = $reverse_summary_index5;

	$hbar6_primary = $summary_index6;
	$hbar6_secondary = $reverse_summary_index6;

	// $summary_index1_padded = sprintf("%02d", $summary_index1);
	// $summary_index2_padded = sprintf("%02d", $summary_index2);
	// $summary_index3_padded = sprintf("%02d", $summary_index3);
	// $summary_index4_padded = sprintf("%02d", $summary_index4);
	// $summary_index5_padded = sprintf("%02d", $summary_index5);
	// $summary_index6_padded = sprintf("%02d", $summary_index6);

	// $reverse_summary_index1_padded = sprintf("%02d", $reverse_summary_index1);
	// $reverse_summary_index2_padded = sprintf("%02d", $reverse_summary_index2);
	// $reverse_summary_index3_padded = sprintf("%02d", $reverse_summary_index3);
	// $reverse_summary_index4_padded = sprintf("%02d", $reverse_summary_index4);
	// $reverse_summary_index5_padded = sprintf("%02d", $reverse_summary_index5);
	// $reverse_summary_index6_padded = sprintf("%02d", $reverse_summary_index6);

	// echo '<h4 class="report__subtitle">Prepared for '.urldecode( $fn ).', born on '.$dateformatted.', and '.urldecode( $fn2 ).', born on '.$dateformatted2.'</h4>';

	// 'free' or 'full'
	include 'common/report-common-report-version.php';

	// ============ Report Charts ============

	if ( $report_part == 'charts' ) {

		if ( $report_version == 'full' ) {

	
			echo '<div class="report__chart-section">';
				echo '<div class="report__chart-description report__chart-description--full-width">';
					echo '<h3 class="report__chart-title"><span>Your Connection Bar Graphs</span></h3>';
					echo '<div class="report__chart-description-wrapper">';
					    echo "<p>Wondering how the two of you match up? Now you can find out, at a glance! The two-way bar graph reveals the chemistry between you and the object of your desires -- how you affect them, and how they affect you -- giving you a quick view of the strength of your connection in six important categories.</p><p>And what are those categories? Just the ones that can make or break a relationship -- like how your core personalities mesh, how the two of you communicate and the levels of love and attraction you feel for each other. In short, have you got what it takes? The Astrologer knows!</p><p>Just remember, even if you both rate high in all six categories, the relationship still might not work out...and even if you rate low, your bond can still grow solid and satisfying. What's best is when a graph shows that two people are about equal in their attraction and connection to each other. That way, no one feels left out! The bar graph can only give you a quick peek at the chemistry of your relationship -- the rest is up to you. Look to your compatibility report for a more in-depth reading of the strengths and challenges of your bond.</p>";
					echo '</div>';
				echo '</div>';
			echo '</div>';

		    echo '<div id="#soulconnection" class="report__chart-section">';
				echo '<h3 class="report__chart-title">Your Soul Connection</h3>';
				echo '<div class="report__chart-section-wrapper">';
					echo '<div class="report__chart-description">';
						echo '<div class="report__chart-description-wrapper">';
							echo "<p class=\"report__read-more\">Your Soul Connection with another person is based on how your Sun connects with their personality, including their values, their outlook and what matters to them, and how their Sun connects with you. Just as the sun is the center of the orbit of all the planets in our solar system, it's also the center of you -- your inner fire, the vital energy that will run through you your whole life. It represents your basic, core personality, separate from all the other influences that drive you. If the lines of the graph – yours is the top line and your sweetie's the bottom -- are very different in length, it means that one of you is going to feel that strong connection but the other won't. If the lines are similar in length that means you'll at least match up, whether you both feel that strong sense of kinship or neither one of you does! Short: Sorry, not really feelin' it. Medium: There are enough differences between you to keep it interesting...for now. Long: You feel like kindred spirits.</p>";
						echo '</div>';
					echo '</div>';
					echo '<div class="report__chart-visuals">';
						echo '<div class="report__chart-animation">';
							include 'common/report-chart-horizontal-two-bars-1.php';
						echo '</div>';
						echo '<div class="report__chart-legends">';
							include 'common/report-chart-horizontal-two-bars-legends.php';
						echo '</div>';
				    echo '</div>';
				echo '</div>';
		    echo '</div>';

		    echo '<div id="#communicativebond" class="report__chart-section">';
				echo '<h3 class="report__chart-title">Your Communicative Bond</h3>';
				echo '<div class="report__chart-section-wrapper">';
					echo '<div class="report__chart-description">';
						echo '<div class="report__chart-description-wrapper">';
							echo "<p class=\"report__read-more\">In this bar graph, your Communicative Bond is based on how your Mercury connects with your crush's world view, their attitude toward relationships and how they express themselves, and on how their Mercury connects with you. Often called the planet of communication, Mercury rules your thought processes, your sense of logic, your intellectual ideas and the way you get those ideas across to the world. If the lines of the graph -- your top line and your sweetie's bottom one -- are very different in length, then one of you will feel mentally turned on and tuned in, but the other just won't feel the connection. If the lines are similar in length, then you're intellectually well-matched, whether that means you'll really get through to each other or that misunderstandings, stand-offs and awkward silences will define your relationship. If your lines are...Short: It's like talking to a brick wall. Medium: Occasional misunderstandings and missed signals. Long: All-night chats -- and plenty of debates!</p>";
						echo '</div>';
					echo '</div>';
					echo '<div class="report__chart-visuals">';
						echo '<div class="report__chart-animation">';
							include 'common/report-chart-horizontal-two-bars-2.php';
						echo '</div>';
						echo '<div class="report__chart-legends">';
							include 'common/report-chart-horizontal-two-bars-legends.php';
						echo '</div>';
				    echo '</div>';
				echo '</div>';
		    echo '</div>';

		    echo '<div id="#lovelink" class="report__chart-section">';
				echo '<h3 class="report__chart-title">Your Love Link</h3>';
				echo '<div class="report__chart-section-wrapper">';
					echo '<div class="report__chart-description">';
						echo '<div class="report__chart-description-wrapper">';
							echo "<p class=\"report__read-more\">In this bar graph, your Love Link with that special someone is based on how Venus, your love planet, connects with them at a heart level, and on how their Venus connects with you. Venus -- the sweet, affectionate and romantic planet of love -- is all about pleasure, bringing people together and uniting them in harmony, so this planet rules not just love and dating but also friendships, partnerships and any social gathering. If the lines of the graph – yours is the top line and your honey's the bottom one -- are very different in length, then one of you is going to get stuck in the limbo of unrequited love. No fun! But if the lines are similar in length, then you're well-matched in terms of affection, whether that means you'll go ga-ga over each other or mutually decide just to be friends. If your lines are...Short: Let's just be friends. Medium: Potential for real affection. Long: Starry-eyed!</p>";
						echo '</div>';
					echo '</div>';
					echo '<div class="report__chart-visuals">';
							echo '<div class="report__chart-animation">';
								include 'common/report-chart-horizontal-two-bars-3.php';
							echo '</div>';
							echo '<div class="report__chart-legends">';
								include 'common/report-chart-horizontal-two-bars-legends.php';
							echo '</div>';
				    echo '</div>';
				echo '</div>';
		    echo '</div>';

		    echo '<div id="#passionateattraction" class="report__chart-section">';
				echo '<h3 class="report__chart-title">Your Passionate Attraction</h3>';
				echo '<div class="report__chart-section-wrapper">';
					echo '<div class="report__chart-description">';
						echo '<div class="report__chart-description-wrapper">';
							echo "<p class=\"report__read-more\">Your Passionate Attraction with your honey is based on how your Mars connects with their sense of who they are, what they go after and how they express themselves, and on how their Mars connects with you in the same way. Mars rules passion, so your sex drive, your propensity for irritation, your urge to compete and succeed -- all of these fall within this powerful planet's realm. Mars affects your attitude toward dating, and it also determines how you argue with your honey. If the lines of the graph – yours is the top line and theirs the botton one -- are very different in length, then one of you is going to feel very passionate and turned on by the relationship, but the other just won't feel that same thrill. If the lines are similar in length, then you'll be well-matched temperamentally, whether you've got a mutually hot bond or one that leaves you both cold! Remember that a strong Passionate Attraction -- both lines nice and long -- means things should be pretty spicy in the bedroom, but that can often come hand-in-hand with a fiery tendency to disagree and debate. Oh well, that just means you get to kiss and make up! If your lines are...Short: Ho-hum. Medium: Some nice thrills. Long: A smokin' hot connection!</p>";
						echo '</div>';
					echo '</div>';
					echo '<div class="report__chart-visuals">';
						echo '<div class="report__chart-animation">';
							include 'common/report-chart-horizontal-two-bars-4.php';
						echo '</div>';
						echo '<div class="report__chart-legends">';
							include 'common/report-chart-horizontal-two-bars-legends.php';
						echo '</div>';
				    echo '</div>';
				echo '</div>';
		    echo '</div>';

		    echo '<div id="#sharedvalues" class="report__chart-section">';
				echo '<h3 class="report__chart-title">Your Shared Values</h3>';
				echo '<div class="report__chart-section-wrapper">';
					echo '<div class="report__chart-description">';
						echo '<div class="report__chart-description-wrapper">';
							echo "<p class=\"report__read-more\">In this bar graph, your Shared Values are based on how your Jupiter connects with your honey's sense of what life is all about, and how their Jupiter connects with you in the same way. Jupiter is referred to as the planet of luck because it brings opportunities and advantages into your life that can seem like a stroke of luck, but are really life lessons meant to test and teach you about your own value system. This planet is also about learning, intellectual pursuits and spirituality -- in short, the expansion of the soul. If the lines of the graph – yours is the top line and your sweetie's the bottom one -- are very different in length, then one of you is going to feel a sense of unity in terms of values and beliefs, but the other will sense plenty of differences between you on those levels. If the lines are similar in length, then at least you match up well -- whether that means you have parallel interests and moral codes or that you couldn't be further apart on that spectrum. If your lines are...Short: Totally opposite moral codes. Medium: Different beliefs make for interesting debates. Long: Agreement on what matters most.</p>";
						echo '</div>';
					echo '</div>';
					echo '<div class="report__chart-visuals">';
						echo '<div class="report__chart-animation">';
							include 'common/report-chart-horizontal-two-bars-5.php';
						echo '</div>';
						echo '<div class="report__chart-legends">';
							include 'common/report-chart-horizontal-two-bars-legends.php';
						echo '</div>';
				    echo '</div>';
				echo '</div>';
		    echo '</div>';

		    echo '<div id="#longtermpotential" class="report__chart-section">';
				echo '<h3 class="report__chart-title">Your Long-Term Potential</h3>';
				echo '<div class="report__chart-section-wrapper">';
					echo '<div class="report__chart-description">';
						echo '<div class="report__chart-description-wrapper">';
							echo "<p class=\"report__read-more\">Your Long-Term Potential with that special someone is based on how your Saturn connects with their needs, their romantic nature and their sense of duty in life, and on how their Saturn connects with you in the same way. Saturn's influence in your chart is like that of your conscience. This planet keeps you in check, reminding you of your responsibilities -- including your commitment to your sweetie. If the lines of the graph – yours is the top line and theirs the bpttom one -- are very different in length, then one of you is going to get deeply involved fast, while the other just won't share that sense of commitment -- not right away, and maybe not ever. If the lines are similar in length, then you're well-matched on that level, whether that means you'll weather life's storms together or both constantly look out for greener grass! If your lines are...Short: Maybe next lifetime. Medium: With some effort, this could last. Long: Is that the sound of wedding bells ringing?</p>";
						echo '</div>';
					echo '</div>';
					echo '<div class="report__chart-visuals">';
						echo '<div class="report__chart-animation">';
							include 'common/report-chart-horizontal-two-bars-6.php';
						echo '</div>';
						echo '<div class="report__chart-legends">';
							include 'common/report-chart-horizontal-two-bars-legends.php';
						echo '</div>';
				    echo '</div>';
				echo '</div>';
		    echo '</div>';

			echo '<div class="report__chart-section">';
				echo '<h3 class="report__chart-title"><span>Your Harmony and Excitement Ratings</span></h3>';
				echo '<div class="report__chart-section-wrapper">';
					echo '<div class="report__chart-description">';
						echo '<div class="report__chart-description-wrapper">';
							echo "<p>The Harmony and Excitement Meters: love can be complex, but The Astrologer can break it down! The Harmony and Excitement Meters reads the compatibility chart between you and your sweetie, and reveals the levels of harmony and excitement in your relationship. Now you'll know at a glance whether things will be smooth sailing, an uphill battle -- or a roller coaster ride between the two!</p><p>Before you jump in, there's a trick to reading the meters. You might think you want a lot of sweet stuff and very little spice, but what you really want is a balance between the two. Just imagine: With all good feelings, pleasant times and nothing to break up the monotony, things would get pretty dull (yawn). And too much passion can also ruin a relationship; without a little breathing room between fights and lusty encounters, you'd tire each other out, fast! Something in the middle is just the ticket. A healthy dose of passion means strong physical attraction plus a good argument every now and then to clear the air, and a sense of trust and understanding means you can always kiss and make up.</p><p>One more word of caution before you get started: Remember, no relationship is perfect. Even with what seems like the ideal balance of harmony and excitement, an affair could turn out to be a total flop. For a much more in-depth reading of the inner workings of your bond, read your full compatibility report from The Astrologer.</p>";
						echo '</div>';
					echo '</div>';
					echo '<div class="report__chart-visuals">';
						echo '<div class="report__chart-animation">';
							include 'common/report-chart-vertical-two-bars.php';
						echo '</div>';
						echo '<div class="report__chart-legends">';

							echo '<div class="report__chart-legends-half-left">';
							    echo '<h4 class="report__chart-legends-title report__chart-legends-title--half-left"">Harmony = '.$harmony.'</h4>';
						        echo '<p><span class="report__chart-legends-score">9-10</span>: So easy its dull</p>';
						        echo '<p><span class="report__chart-legends-score">7-8</span>: sweet as candy</p>';
						        echo '<p><span class="report__chart-legends-score">4-6</span>: a real affinity</p>';
						        echo '<p><span class="report__chart-legends-score">1-3</span>: A nice rapport!</p>';
							echo '</div>';

							echo '<div class="report__chart-legends-half-right">';
							    echo '<h4 class="report__chart-legends-title report__chart-legends-title--half-right">Excitement = '.$excitement.'</h4>';
						        echo '<p><span class="report__chart-legends-score">9-10</span>: War Zone</p>';
						        echo '<p><span class="report__chart-legends-score">7-8</span>: Over the edge</p>';
						        echo '<p><span class="report__chart-legends-score">4-6</span>: Hot,hot,hot</p>';
						        echo '<p><span class="report__chart-legends-score">1-3</span>: Healthy</p>';
							echo '</div>';

						echo '</div>';
					echo '</div>';

				echo '</div>';
			echo '</div>';

			include 'common/report-common-read-more-less-js.php';

		} else {

			include 'common/report-common-charts-preview.php';

		}

	} else {}

	// ============ Report Articles ============

	if ( $report_part == 'articles' ) {

		include 'common/report-free-limits-start.php';

		foreach ( $report_content_raw->detail[0]->section as $section ) {

			if ( $report_version == 'free' ) {

				if ( $i == $limit ) break;

			} else {}

			echo '<div class="report__section">';
			echo '<h2 class="report__section-title"><span>'.$section->title.'</span></h2>';

			foreach ( $section->paragraph as $paragraph ) {

		      	include 'common/report-common-content-interaspect.php';

				include 'common/report-image-pool-complete.php';

				echo '<div class="report__article">';

		        if ( $planet1 == 'north' ) {

					$planet1 = 'northnode';
					$aspect = strtolower($interaspctchunk[2]);
					$planet2 = strtolower($interaspctchunk[3]);

		        }

		        if ( empty($planet1) ) {

		            echo '<h4 class="report__article-title">'.$heading.'</h4>';

					include 'common/report-common-variation-content.php';
		 
		        } else {

					if ( $aspect == 'in' ) {

			            echo '<span class="report__article-subtitle"><img src="/wp-content/uploads/images/glyphs/'.$planet1.'_white.png" /><img src="/wp-content/uploads/images/glyphs/'.$planet2.'_white.png" />'.$interaspect.'</span>';

			            echo '<h4 class="report__article-title">'.$heading.'</h4>';

					} else {

			            echo '<span class="report__article-subtitle"><img src="/wp-content/uploads/images/glyphs/'.$planet1.'_white.png" /><img src="/wp-content/uploads/images/glyphs/'.$aspect.'_white.png" /><img src="/wp-content/uploads/images/glyphs/'.$planet2.'_white.png" />'.$interaspect.'</span>';

			            echo '<h4 class="report__article-title">'.$heading.'</h4>';

					}

					include 'common/report-common-variation-content.php';

		        }

				echo '</div>';

			}

			echo '</div>';

			include 'common/report-free-limits-end.php';
		  
		}

	} else {}

}