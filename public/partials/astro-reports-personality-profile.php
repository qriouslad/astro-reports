<?php

$report_name = 'personality-profile';

include 'common/report-common-one-person.php';

if ( empty ( $fn ) || empty ( $id ) ) {

	include 'common/report-not-found.php';

} else {

	include 'common/report-common-site-date-info.php';

	include 'common/report-common-variation.php';

	$personality_profile_data_cachekey = 'pp_'.$atts['variation'].'_'.$id;

	$personality_profile_data_cached = simplexml_load_string( get_transient( $personality_profile_data_cachekey ) );

	if ( false === $personality_profile_data_cached ) {

		$url = "https://www.zdki.us/taReportsw/MakeReport.aspx?ReportID=41&ReportVariation=".$report_variation."&ReportFormat=XML&Name1=".$fn."&BirthDate1=".$datetime."&Name2=&BirthDate2=&StartDate=&Length=&AccountID=".$accountid."&AppID=astroreports_NUM&MemberID=numerologist1&V=2";

		// defines $data variable with API's response
		include 'common/report-common-http-auth.php';

		$report_content_raw = simplexml_load_string($data);

		set_transient( $personality_profile_data_cachekey, $report_content_raw->asXML(), MONTH_IN_SECONDS );

		// echo '<h6>Uncached data is used</h6>';

	} else {

		$report_content_raw = $personality_profile_data_cached;

		// echo '<h6>Cached data is used. Cache key: '.$personality_profile_data_cachekey.'</h6>';

	}

	// Get bar chart data from Romantic Personality Profile API

	$romantic_personality_profile_data_cachekey = 'rpp_'.$atts['variation'].'_'.$id;

	$romantic_personality_profile_data_cached = simplexml_load_string( get_transient( $romantic_personality_profile_data_cachekey ) );

	if ( false === $romantic_personality_profile_data_cached ) {

		$alt_url = "https://www.zdki.us/taReportsw/MakeReport.aspx?ReportID=15&ReportVariation=".$report_variation."&ReportFormat=XML&Name1=".$fn."&BirthDate1=".$datetime."&Name2=&BirthDate2=&StartDate=&Length=&AccountID=".$accountid."&AppID=astroreports_NUM&MemberID=numerologist1&V=2";


		// defines $data variable with API's response
		// include 'common/report-common-http-auth.php';

		$username = 'atv';
		$password = 'sgsr43%^vcrgSF';
		$context = stream_context_create(array(
				'http' => array( 'header'  => "Authorization: Basic " . base64_encode("$username:$password") )
			)
		);
		$alt_data = file_get_contents($alt_url, false, $context);

		$alt_report_content_raw = simplexml_load_string($alt_data);

		set_transient( $romantic_personality_profile_data_cachekey, $alt_report_content_raw->asXML(), MONTH_IN_SECONDS );

		// echo '<h6>Uncached data is used</h6>';

	} else {

		$alt_report_content_raw = $romantic_personality_profile_data_cached;

		// echo '<h6>Cached data is used. Cache key: '.$romantic_personality_profile_data_cachekey.'</h6>';

	}

	$romantic_nature = $alt_report_content_raw->summary[0]->venusindex;
	$passionate_nature = $alt_report_content_raw->summary[0]->marsindex;

	$vbar1_score = $romantic_nature;
	$vbar2_score = $passionate_nature;

	// echo '<h4 class="report__subtitle">Prepared for '.urldecode( $fn ).', born on '.$dateformatted.'</h4>';

	// 'free' or 'full'
	include 'common/report-common-report-version.php';

	// ============ Report Charts ============

	if ( $report_part == 'charts' ) {

		if ( $report_version == 'full' ) {

		    echo'<div class="report__chart-section">';
				echo '<h3 class="report__chart-title"><span>Your Nature</span></h3>';
				echo '<div class="report__chart-section-wrapper">';

					echo '<div class="report__chart-description">';
						echo '<div class="report__chart-description-wrapper">';
							echo "<p>Description needed here... Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.</p><p>Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc.</p>";
						echo '</div>';
					echo '</div>';

					echo '<div class="report__chart-visuals">';
						echo '<div class="report__chart-animation">';
							include 'common/report-chart-vertical-two-bars.php';
						echo '</div>';
						echo '<div class="report__chart-legends">';
							echo '<div class="report__chart-legends-half-left">';
							    echo '<h4 class="report__chart-legends-title report__chart-legends-title--half-left">What Nature? = '.$romantic_nature.'</h4>';
								echo '<p><span class="report__chart-legends-score">7-10</span> Legend needed here...</p>';
								echo '<p><span class="report__chart-legends-score">4-6</span> Legend needed here...</p>';
								echo '<p><span class="report__chart-legends-score">1-3</span> Legend needed here...</p>';
							echo '</div>';
							echo '<div class="report__chart-legends-half-right">';
							    echo '<h4 class="report__chart-legends-title report__chart-legends-title--half-right">What Nature? = '.$passionate_nature.'</h4>';
								echo '<p><span class="report__chart-legends-score">7-10</span>Legend needed here...</p>';
								echo '<p><span class="report__chart-legends-score">4-6</span>Legend needed here...</p>';
								echo '<p><span class="report__chart-legends-score">1-3</span>Legend needed here...</p>';
							echo '</div>';
						echo '</div>';
					echo '</div>';
	
			    echo '</div>';
		    echo '</div>';
		} else {

			include 'common/report-common-charts-preview.php';

		}

	} else {}

	// ============ Report Articles ============

	if ( $report_part == 'articles' ) {

		include 'common/report-free-limits-start.php';

		foreach ( $report_content_raw->detail[0]->section as $section ) {

			if ( $report_version == 'free' ) {

				if ( $i == $limit ) break;

			} else {}

			foreach ( $section->paragraph as $paragraph ) {

				$interaspect = $paragraph->interaspect;
				$interaspctchunk = explode(" ",$interaspect);
				$planet1 = strtolower($interaspctchunk[0]);

			}

			echo'<div class="report__section">';
			echo '<h2 class="report__section-title"><span><img src="/wp-content/uploads/images/glyphs/'.$planet1.'_white.png" />'.$section->title.'</span></h2>';

			foreach ( $section->paragraph as $paragraph ) {

				include 'common/report-common-content-interaspect.php';

				include 'common/report-image-pool-partial.php';

				echo '<div class="report__article">';

				if ( $planet1 == 'north' ) {

					$planet1 = 'northnode';
					$aspect = strtolower($interaspctchunk[2]);
					$planet2 = strtolower($interaspctchunk[3]);

				}

				if ( $aspect == 'in' ) {

					echo '<span class="report__article-subtitle"><img src="/wp-content/uploads/images/glyphs/'.$planet1.'_white.png" /><img src="/wp-content/uploads/images/glyphs/'.$planet2.'_white.png" />'.$interaspect.'</span>';

					echo '<h4 class="report__article-title">'.$heading.'</h4>';

				} else {

					echo '<span class="report__article-subtitle"><img src="/wp-content/uploads/images/glyphs/'.$planet1.'_white.png" /><img src="/wp-content/uploads/images/glyphs/'.$aspect.'_white.png" /><img src="/wp-content/uploads/images/glyphs/'.$planet2.'_white.png" />'.$interaspect.'</span>';

					echo '<h4 class="report__article-title">'.$heading.'</h4>';

				}

				include 'common/report-common-variation-content.php';

				echo'</div>';

			}

			echo'</div>';

			include 'common/report-free-limits-end.php';

		}

	} else {}

}