<?php

// Karmic Compatibility (Karma and Destiny Compatibility)

$report_name = 'karmic-compatibility';

include 'common/report-common-two-person.php';

if ( empty ( $fn ) || empty ( $id ) || empty ( $fn2 ) || empty ( $id2 ) ) {

	include 'common/report-not-found.php';

} else {

	include 'common/report-common-site-date-info.php';

	include 'common/report-common-variation.php';

	$karma_destiny_data_cachekey = 'kd_'.$atts['variation'].'_'.$id.'_'.$id2;

	$karma_destiny_data_cached = simplexml_load_string( get_transient( $karma_destiny_data_cachekey ) );

	if ( false === $karma_destiny_data_cached ) {

		$url = "https://www.zdki.us/taReportsw/MakeReport.aspx?ReportID=52&ReportVariation=".$report_variation."&ReportFormat=XML&Name1=".$fn."&BirthDate1=".$datetime."&Name2=".$fn2."&BirthDate2=".$datetime2."&StartDate=&Length=&AccountID=".$accountid."&AppID=astroreports_NUM&MemberID=numerologist1&V=2";

		// defines $data variable with API's response
		include 'common/report-common-http-auth.php';

		$report_content_raw = simplexml_load_string($data);

		set_transient( $karma_destiny_data_cachekey, $report_content_raw->asXML(), MONTH_IN_SECONDS );

		// echo '<h6>Uncached data is used</h6>';

	} else {

		$report_content_raw = $karma_destiny_data_cached;

		// echo '<h6>Cached data is used. Cache key: '.$karma_destiny_data_cachekey.'</h6>';

	}

	$harmony = $report_content_raw->summary[0]->harmonyindex;
	$challenge = $report_content_raw->summary[0]->challengeindex;

	$vbar1_score = $harmony;
	$vbar2_score = $challenge;

	// $harmony_padded = sprintf("%02d", $harmony);
	// $challenge_padded = sprintf("%02d", $challenge);

	$summary_index1 = $report_content_raw->summary[0]->chaptersum[0]->index;
	$summary_index2 = $report_content_raw->summary[0]->chaptersum[1]->index;
	$summary_index3 = $report_content_raw->summary[0]->chaptersum[2]->index;
	$summary_index4 = $report_content_raw->summary[0]->chaptersum[3]->index;
	$summary_index5 = $report_content_raw->summary[0]->chaptersum[4]->index;
	$summary_index6 = $report_content_raw->summary[0]->chaptersum[5]->index;

	$reverse_summary_index1 = $report_content_raw->reverse_summary[0]->chaptersum[0]->index;
	$reverse_summary_index2 = $report_content_raw->reverse_summary[0]->chaptersum[1]->index;
	$reverse_summary_index3 = $report_content_raw->reverse_summary[0]->chaptersum[2]->index;
	$reverse_summary_index4 = $report_content_raw->reverse_summary[0]->chaptersum[3]->index;
	$reverse_summary_index5 = $report_content_raw->reverse_summary[0]->chaptersum[4]->index;
	$reverse_summary_index6 = $report_content_raw->reverse_summary[0]->chaptersum[5]->index;

	$hbar1_primary = $summary_index1;
	$hbar1_secondary = $reverse_summary_index1;

	$hbar2_primary = $summary_index2;
	$hbar2_secondary = $reverse_summary_index2;

	$hbar3_primary = $summary_index3;
	$hbar3_secondary = $reverse_summary_index3;

	$hbar4_primary = $summary_index4;
	$hbar4_secondary = $reverse_summary_index4;

	$hbar5_primary = $summary_index5;
	$hbar5_secondary = $reverse_summary_index5;

	$hbar6_primary = $summary_index6;
	$hbar6_secondary = $reverse_summary_index6;

	// $summary_index1_padded = sprintf("%02d", $summary_index1);
	// $summary_index2_padded = sprintf("%02d", $summary_index2);
	// $summary_index3_padded = sprintf("%02d", $summary_index3);
	// $summary_index4_padded = sprintf("%02d", $summary_index4);
	// $summary_index5_padded = sprintf("%02d", $summary_index5);
	// $summary_index6_padded = sprintf("%02d", $summary_index6);

	// $reverse_summary_index1_padded = sprintf("%02d", $reverse_summary_index1);
	// $reverse_summary_index2_padded = sprintf("%02d", $reverse_summary_index2);
	// $reverse_summary_index3_padded = sprintf("%02d", $reverse_summary_index3);
	// $reverse_summary_index4_padded = sprintf("%02d", $reverse_summary_index4);
	// $reverse_summary_index5_padded = sprintf("%02d", $reverse_summary_index5);
	// $reverse_summary_index6_padded = sprintf("%02d", $reverse_summary_index6);

	// echo '<h4 class="report__subtitle">Prepared for '.urldecode( $fn ).', born on '.$dateformatted.', and '.urldecode( $fn2 ).', born on '.$dateformatted2.'</h4>';

	// 'free' or 'full'
	include 'common/report-common-report-version.php';

	// ============ Report Charts ============

	if ( $report_part == 'charts' ) {

		if ( $report_version == 'full' ) {

			echo '<div class="report__chart-section">';
				echo '<div class="report__chart-description report__chart-description--full-width">';
					echo '<h3 class="report__chart-title"><span>Your Connection Bar Graphs</span></h3>';
					echo '<div class="report__chart-description-wrapper">';
						echo "<p>Wondering how the two of you have been intertwined throughout lifetimes? Now you can find out, at a glance! Our two-way bar graph reveals the kismet kinship between you and your twin soul -- how you affect them, and how they affect you -- giving you a quick view of the strength of your connection in six important categories.</p><p>And what are those categories? Just the ones that can make or break a bond -- like how your souls connect, how the two of you communicate in this lifetime and prior lifetimes and the levels of karma you have for each other. In short, have you got what it takes to continue your cosmic connection or is it time to break free? The Astrologer knows!</p><p>Just remember, even if you both rate high in all six categories, your bond might break this lifetime...and even if you rate low, you may still be here to learn lessons from each other. What's best is when a graph shows that two people are about equal in their cosmic connection to each other. That way, no one feels left out! The bar graphs can only give you a quick peek at the chemistry of your karma and destiny-- the rest is up to you. </p>";
					echo '</div>';
				echo '</div>';
			echo '</div>';

			echo '<div id="#sun" class="report__chart-section">';
				echo '<h3 class="report__chart-title">Sun - Soul to Soul</h3>';
				echo '<div class="report__chart-section-wrapper">';
					echo '<div class="report__chart-description">';
						echo '<div class="report__chart-description-wrapper">';
							echo "<p class=\"report__read-more\">Your Soul Connection with your foreordained friend is based on how your Sun connects with them on a Soul level, including their values, their outlook and their experiences brought over from past lives, and how their Sun connects with you. Just as the Sun is the center of the orbit of all the planets in our solar system, it's also the center of you -- your life lessons, the vital energy that will run through you your whole life. It represents your basic, core personality from lifetimes of experience, separate from all the other influences that drive you. If the lines of the graph – yours are the lines on the top and your cosmic buddy’s are the lines on the bottom -- are very different in length, it means that one of you is going to feel that familiar past life connection but the other won't. If the lines are similar in length that means you'll at least match up, whether you both feel that strong sense of fate or neither one of you does! If your lines are...Long: You feel like twin flames. Medium: There is still karma to work through. Short: Perhaps this is your first lifetime together.</p>";
						echo '</div>';
					echo '</div>';
					echo '<div class="report__chart-visuals">';
						echo '<div class="report__chart-animation">';
							include 'common/report-chart-horizontal-two-bars-1.php';
						echo '</div>';
						echo '<div class="report__chart-legends">';
							include 'common/report-chart-horizontal-two-bars-legends.php';
						echo '</div>';
					echo '</div>';
				echo '</div>';
			echo '</div>';

			echo '<div id="#mercury" class="report__chart-section">';
				echo '<h3 class="report__chart-title">Mercury - Karmic Conversations</h3>';
				echo '<div class="report__chart-section-wrapper">';
					echo '<div class="report__chart-description">';
						echo '<div class="report__chart-description-wrapper">';
							echo "<p class=\"report__read-more\">In this bar graph, your Communicative Bond is based on how your Mercury connects with your moirai mate’s world view in this lifetime and previous lives, how they express themselves, and on how their Mercury connects with you. Often called the planet of communication, Mercury rules your thought processes, your sense of logic, your intellectual ideas and the way you get those ideas across to the world, now and in the past. If the lines of the graph – yours is the top one while your cosmic buddy’s is the bottom one are very different in length, then one of you will feel mentally turned on and tuned in, but the other just won't feel the connection. If the lines are similar in length, then you're intellectually well-matched, whether that means you'll really get through to each other as you may have done previously or that misunderstandings, stand-offs and past karma will define your destiny. If your lines are...Long: You know what the other is thinking with just a look. Medium: More work is needed to be understood. Short: It’s like you’re sometimes speaking another language.</p>";
						echo '</div>';
					echo '</div>';
					echo '<div class="report__chart-visuals">';
						echo '<div class="report__chart-animation">';
							include 'common/report-chart-horizontal-two-bars-2.php';
						echo '</div>';
						echo '<div class="report__chart-legends">';
							include 'common/report-chart-horizontal-two-bars-legends.php';
						echo '</div>';
					echo '</div>';
				echo '</div>';
			echo '</div>';

			echo '<div id="#venus" class="report__chart-section">';
				echo '<h3 class="report__chart-title">Venus - Soul Mates?</h3>';
				echo '<div class="report__chart-section-wrapper">';
					echo '<div class="report__chart-description">';
						echo '<div class="report__chart-description-wrapper">';
							echo "<p class=\"report__read-more\">In this bar graph, your link with your twin flame is based on how Venus, your love planet, connects with them at a heart level, and on how their Venus connects with you, now and in the past. Venus -- the sweet and affectionate planet of love, harmony and socializing -- is all about pleasure, bringing people together and uniting them in harmony, so this planet rules not just love and dating but also friendships, partnerships and any social gathering. If the lines of the graph – yours are the top one’s and your cosmic kin’s are the bottom lines -- are very different in length, then one of you is going to get stuck in the limbo of unrequited and even unresolved love. No fun! But if the lines are similar in length, then you're well-matched in terms of affection. If your lines are...Long: Long lost lovers. Medium: Potential to resolve issues with matters of the heart. Short: Not much to work through in terms of the past.</p>";
						echo '</div>';
					echo '</div>';
					echo '<div class="report__chart-visuals">';
						echo '<div class="report__chart-animation">';
							include 'common/report-chart-horizontal-two-bars-3.php';
						echo '</div>';
						echo '<div class="report__chart-legends">';
							include 'common/report-chart-horizontal-two-bars-legends.php';
						echo '</div>';
					echo '</div>';
				echo '</div>';
			echo '</div>';

			echo '<div id="#mars" class="report__chart-section">';
				echo '<h3 class="report__chart-title">Mars - Past Life Passion</h3>';
				echo '<div class="report__chart-section-wrapper">';
					echo '<div class="report__chart-description">';
						echo '<div class="report__chart-description-wrapper">';
							echo "<p class=\"report__read-more\">The excitement and passion you have with your twin flame is based on how your Mars connects with their sense of who they are and who they were, what they go after and how they express themselves now and in previous lives. It’s also how their Mars connects with you in the same way. Mars rules action, your propensity for irritation, your urge to compete and succeed -- all of these fall within this powerful planet's realm. Mars affects your attitude toward passion, and it also determines how you could argue with your mate. It’s also the experiences we bring with us from the past that determine our level of passion in this lifetime. If the lines of the graph – yours are on the top and theirs on the bottom-- are very different in length, then one of you is going to feel very driven by the relationship, but the other just won't feel that same thrill. If the lines are similar in length, then you'll be well-matched temperamentally, whether you've got a mutually strong bond that has been unbroken across lifetimes or one that leaves you both cold and ready to move on. If your lines are...Long: So much passion across lifetimes. Medium: Good times and memories. Short: A little too dull.</p>";
						echo '</div>';
					echo '</div>';
					echo '<div class="report__chart-visuals">';
						echo '<div class="report__chart-animation">';
							include 'common/report-chart-horizontal-two-bars-4.php';
						echo '</div>';
						echo '<div class="report__chart-legends">';
							include 'common/report-chart-horizontal-two-bars-legends.php';
						echo '</div>';
					echo '</div>';
				echo '</div>';
			echo '</div>';

			echo '<div id="#jupiter" class="report__chart-section">';
				echo '<h3 class="report__chart-title">Jupiter - Faith in Each Other</h3>';
				echo '<div class="report__chart-section-wrapper">';
					echo '<div class="report__chart-description">';
						echo '<div class="report__chart-description-wrapper">';
							echo "<p class=\"report__read-more\">In this bar graph, your Faith, Beliefs and Shared Values are based on how your Jupiter connects with your predestine pal’s sense of what life is all about and perhaps what it was, and how their Jupiter connects with you in the same way. Jupiter is referred to as the planet of luck because it brings opportunities and advantages into your life that can seem like a stroke of luck, but are really life lessons meant to test and teach you about your own value system. This planet is also about learning, intellectual pursuits and spirituality -- in short, the expansion of the soul. If the lines of the graph – yours are the top one’s and your karmic buddys are the bottom graphs-- are very different in length, then one of you is going to feel a sense of unity in terms of values and beliefs, but the other will sense plenty of differences between you on those levels. If the lines are similar in length, then at least you match up well -- whether that means you have parallel interests or even parallel lives and moral codes from now and the past or that you couldn't be further apart on that spectrum. If your lines are...Long: Agreement on what matters most, that old familiar feeling. Medium: Agree to disagree so you can both move forward. Short: Totally opposite moral codes and lots of issues to be resolved.</p>";
						echo '</div>';
					echo '</div>';
					echo '<div class="report__chart-visuals">';
						echo '<div class="report__chart-animation">';
							include 'common/report-chart-horizontal-two-bars-5.php';
						echo '</div>';
						echo '<div class="report__chart-legends">';
							include 'common/report-chart-horizontal-two-bars-legends.php';
						echo '</div>';
					echo '</div>';
				echo '</div>';
			echo '</div>';

			echo '<div id="#saturn" class="report__chart-section">';
				echo '<h3 class="report__chart-title">Saturn - Lessons Learned</h3>';
				echo '<div class="report__chart-section-wrapper">';
					echo '<div class="report__chart-description">';
						echo '<div class="report__chart-description-wrapper">';
							echo "<p class=\"report__read-more\">Your Long-Term Potential to become mates for life is based on how your Saturn connects with their needs, their nature and their sense of duty in this and past lives, and on how their Saturn connects with you in the same way. Saturn's influence in your chart is like that of your conscience. This planet keeps you in check, reminding you of your responsibilities -- including your commitment to your kismet kin. If the lines of the graph – yours are the top one’s and theirs the bottom-- are very different in length, then one of you is going to recognize the past life bond, while the other just won't share that sense of fate -- not right away, and maybe not ever. If the lines are similar in length, then you're well-matched on that level, whether that means you'll be trying to work through your destiny together or have to wait until next lifetime. If your lines are...Long: Karma to work through over many lifetimes. Medium: It may take more lives before issues are resolved. Short: Starting afresh, perhaps this is a new cosmic connection.</p>";
						echo '</div>';
					echo '</div>';
					echo '<div class="report__chart-visuals">';
						echo '<div class="report__chart-animation">';
							include 'common/report-chart-horizontal-two-bars-6.php';
						echo '</div>';
						echo '<div class="report__chart-legends">';
							include 'common/report-chart-horizontal-two-bars-legends.php';
						echo '</div>';
					echo '</div>';
				echo '</div>';
			echo '</div>';

			echo '<div class="report__chart-section">';
				echo '<h3 class="report__chart-title"><span>Your Harmony and Challenge Ratings</span></h3>';
				echo '<div class="report__chart-section-wrapper">';
					echo '<div class="report__chart-description">';
						echo '<div class="report__chart-description-wrapper">';
							echo "<p>The Harmony and Challenge Meters read the compatibility chart between you and your kindred soul, and reveals the levels of harmony and challenges in your relationship. Now you'll know at a glance whether things will feel familiar, be constant angst-- or too much water under the bridge between you two!</p><p>Before you jump in, there's a trick to reading the meters. You might think you want a lot of harmony and very little challenges, but what you really want is a mix between the two. Just imagine: With old familiar feelings, pleasant times and nothing to break up the monotony, things would get pretty dull (yawn). And too many challenges can also ruin a relationship; without a little breathing room between fights and disagreements you'd tire each other out, fast! Something in the middle is just the ticket. Sometimes a good argument every now and then to clear the air, and a sense of trust and understanding means you will always have each other’s back and you can both move forward with your destiny.</p><p>One more word of caution before you get started: Remember, no relationship perfect. Even with what seems like the ideal balance of harmony and challenge, a relationship could turn out to be a total flop. For a much more in-depth reading of the inner workings of your cosmic bond, read your full Karma and Destiny Compatibility report.</p>";
						echo '</div>';
					echo '</div>';
					echo '<div class="report__chart-visuals">';
						echo '<div class="report__chart-animation">';
							include 'common/report-chart-vertical-two-bars.php';
						echo '</div>';
						echo '<div class="report__chart-legends">';

							echo '<div class="report__chart-legends-half-left">';
							    echo '<h4 class="report__chart-legends-title report__chart-legends-title--half-left"">Harmony = '.$harmony.'</h4>';
								echo '<p><span class="report__chart-legends-score">9-10</span>: Twin flames?</p>';
								echo '<p><span class="report__chart-legends-score">7-8</span>: S comforting connection</p>';
								echo '<p><span class="report__chart-legends-score">4-6</span>: Sometimes your relationship feels like a pair of old slippers</p>';
								echo '<p><span class="report__chart-legends-score">1-3</span>: Relationships shouldn’t be too much hard work</p>';
							echo '</div>';

							echo '<div class="report__chart-legends-half-right">';
							    echo '<h4 class="report__chart-legends-title report__chart-legends-title--half-right">Challenge = '.$challenge.'</h4>';
								echo '<p><span class="report__chart-legends-score">9-10</span>: It might be time to learn your lessons from each other and move on</p>';
								echo '<p><span class="report__chart-legends-score">7-8</span>: You have something to learn from each other</p>';
								echo '<p><span class="report__chart-legends-score">4-6</span>: A hiccup here and there between you but nothing life shattering</p>';
								echo '<p><span class="report__chart-legends-score">1-3</span>: Sometimes a challenge can be a good thing!</p>';
							echo '</div>';

						echo '</div>';
					echo '</div>';

				echo '</div>';
			echo '</div>';

			include 'common/report-common-read-more-less-js.php';

		} else {

			include 'common/report-common-charts-preview.php';

		}

	} else {}

	// ============ Report Articles ============

	if ( $report_part == 'articles' ) {

		include 'common/report-free-limits-start.php';

		foreach ( $report_content_raw->detail[0]->chapter as $chapter ) {

			if ( $report_version == 'free' ) {

				if ( $i == $limit ) break;

			} else {}

			echo '<div class="report__section">';
			echo '<h2 class="report__section-title"><span>'.$chapter->title.'</span></h2>';

			foreach ( $chapter->paragraph as $paragraph ) {

				include 'common/report-common-content-interaspect.php';

				$heading_lower = strtolower($heading);

				if ( $planet1 == 'north' ) {

					$planet1 = 'northnode';
					$aspect = strtolower($interaspctchunk[2]);
					$planet2 = strtolower($interaspctchunk[3]);

				}

				include 'common/report-image-pool-partial.php';

				echo '<div class="report__article">';

				if ( empty($planet1) ) {

					if ( $heading_lower == 'totally different personalities' ) {

						$interaspect = 'No Sun Aspects';
						$planet1 = 'sun';

					} elseif ( $heading_lower == 'falling on deaf ears' ) {

						$interaspect = 'No Mercury Aspects';
						$planet1 = 'mercury';

					} elseif ( $heading_lower == 'a lack of attraction' ) {

						$interaspect = 'No Venus Aspects';
						$planet1 = 'venus';

					} elseif ( $heading_lower == "where's the passion?" ) {

						$interaspect = 'No Mars Aspects';
						$planet1 = 'mars';

					} elseif ( $heading_lower == 'a different value system' ) {

						$interaspect = 'No Jupiter Aspects';
						$planet1 = 'jupiter';

					} elseif ( $heading_lower == 'look elsewhere for long-term stability' ) {

						$interaspect = 'No Saturn Aspects';
						$planet1 = 'saturn';

					}

				    echo '<span class="report__article-subtitle"><img src="/wp-content/uploads/images/glyphs/unaspected_white.png" /><img src="/wp-content/uploads/images/glyphs/'.$planet1.'_white.png" />'.$interaspect.'</span>';
				    echo '<h4 class="report__article-title">'.$heading.'</h4>';

					include 'common/report-common-variation-content.php';

				} else {

					if ( $aspect == 'in' ) {

						echo '<span class="report__article-subtitle"><img src="/wp-content/uploads/images/glyphs/'.$planet1.'_white.png" /><img src="/wp-content/uploads/images/glyphs/'.$planet2.'_white.png" />'.$interaspect.'</span>';

						echo '<h4 class="report__article-title">'.$heading.'</h4>';

					} else {

						echo '<span class="report__article-subtitle"><img src="/wp-content/uploads/images/glyphs/'.$planet1.'_white.png" /><img src="/wp-content/uploads/images/glyphs/'.$aspect.'_white.png" /><img src="/wp-content/uploads/images/glyphs/'.$planet2.'_white.png" />'.$interaspect.'</span>';

						echo '<h4 class="report__article-title">'.$heading.'</h4>';

					}

					include 'common/report-common-variation-content.php';

				}

				echo '</div>';

			}

			echo '</div>';

			include 'common/report-free-limits-end.php';

		}

	} else {}

}