<?php

// Relationship Remedy (Relationship Wrongs)

$report_name = 'relationship-remedy'; 

include 'common/report-common-one-person.php';

if ( empty ( $fn ) || empty ( $id ) ) {

	include 'common/report-not-found.php';

} else {

	include 'common/report-common-site-date-info.php';

	include 'common/report-common-variation.php';

	$relationship_wrongs_data_cachekey = 'rw_'.$atts['variation'].'_'.$id;

	$relationship_wrongs_data_cached = simplexml_load_string( get_transient( $relationship_wrongs_data_cachekey ) );

	if ( false === $relationship_wrongs_data_cached ) {

		$url = "https://www.zdki.us/taReportsw/MakeReport.aspx?ReportID=43&ReportVariation=".$report_variation."&ReportFormat=XML&Name1=".$fn."&BirthDate1=".$datetime."&Name2=&BirthDate2=&StartDate=&Length=&AccountID=".$accountid."&AppID=astroreports_NUM&MemberID=numerologist1&V=2";

		// defines $data variable with API's response
		include 'common/report-common-http-auth.php';

		$report_content_raw = simplexml_load_string($data);

		set_transient( $relationship_wrongs_data_cachekey, $report_content_raw->asXML(), MONTH_IN_SECONDS );

		// echo '<h6>Uncached data is used</h6>';

	} else {

		$report_content_raw = $relationship_wrongs_data_cached;

		// echo '<h6>Cached data is used. Cache key: '.$relationship_wrongs_data_cachekey.'</h6>';

	}

	$innerwarzone_meter = $report_content_raw->summary[0]->innerwarzone_meter;
	$projections_meter = $report_content_raw->summary[0]->projections_meter;
	$relationship_meter = $report_content_raw->summary[0]->relationship_meter;

	$vbar1_score = $innerwarzone_meter;
	$vbar2_score = $projections_meter;
	$vbar3_score = $relationship_meter;

	// $innerwarzone_meter_padded = sprintf("%02d", $innerwarzone_meter);
	// $projections_meter_padded = sprintf("%02d", $projections_meter);
	// $relationship_meter_padded = sprintf("%02d", $relationship_meter);

	// echo '<h4 class="report__subtitle">Prepared for '.urldecode( $fn ).', born on '.$dateformatted.'</h4>';

	// 'free' or 'full'
	include 'common/report-common-report-version.php';

	// ============ Report Charts ============

	if ( $report_part == 'charts' ) {

		if ( $report_version == 'full' ) {

		    echo'<div class="report__chart-section">';
			    echo '<h3 class="report__chart-title"><span>Your Nature</span></h3>';
				echo '<div class="report__chart-section-wrapper">';

					echo '<div class="report__chart-description">';
						echo '<div class="report__chart-description-wrapper">';
						    echo "<p>The Inner War Zone, Projections and Relationship Karma Meters: so, you want the nitty-gritty, right? You're ready to know the real deal when it comes to your own true nature and psychological issues -- the ones that keep you from finding and holding on to that ideal love.</p><p>Reading your Relationship Remedy report will give you that in-depth picture, but these three meters show an at-a-glance view of the lessons you have yet to learn. Remember, whether your scores on the meters are high or low, you're in good company: Most, if not all, of us have plenty of emotional baggage that comes from past relationships, our childhoods, even past lives. And we all have room for improvement when it comes to changing our lives for the better and improving our relationships, including our love connections.</p>";
						echo '</div>';
					echo '</div>';

					echo '<div class="report__chart-visuals">';
						echo '<div class="report__chart-animation">';
							include 'common/report-chart-vertical-three-bars.php';
						echo '</div>';
						echo '<div class="report__chart-legends report__chart-legends--thirds">';

							echo '<div class="report__chart-legends-third-left">';
							    echo '<h4 class="report__chart-legends-title report__chart-legends-title--third report__chart-legends-title-third-left">Inner War Zone = '.$innerwarzone_meter.'</h4>';
								echo '<p><span class="report__chart-legends-score">7-10</span> Repressed anger, defensiveness and a seriously short fuse can turn your love affairs into out-and-out war.</p>';
								echo '<p><span class="report__chart-legends-score">4-6</span> You\'ve got some issues and inner conflicts to work through, but who doesn\'t?</p>';
								echo '<p><span class="report__chart-legends-score">1-3</span> You\'re so laid back in your relationships it could actually be a problem.</p>';
							echo '</div>';

							echo '<div class="report__chart-legends-third-center">';
							    echo '<h4 class="report__chart-legends-title report__chart-legends-title--third report__chart-legends-title-third-center">Projections = '.$projections_meter.'</h4>';
								echo '<p><span class="report__chart-legends-score">7-10</span> Seeing all sides of every issue is actually a detriment. You don\'t know what to believe!</p>';
								echo '<p><span class="report__chart-legends-score">4-6</span> You know your own mind, but you can see their point too. That\'s a good thing.</p>';
								echo '<p><span class="report__chart-legends-score">1-3</span> You see your sweetie as they really are...but do you understand them?</p>';
							echo '</div>';

							echo '<div class="report__chart-legends-third-right">';
							    echo '<h4 class="report__chart-legends-title report__chart-legends-title--third report__chart-legends-title-third-right">Relationship Karma = '.$relationship_meter.'</h4>';
								echo '<p><span class="report__chart-legends-score">7-10</span> It\'s past time to work through your baggage from past affairs, or even past lives. Try moving forward!</p>';
								echo '<p><span class="report__chart-legends-score">4-6</span> Some of your relationship issues have been around for a while. It\'s time to sort them out.</p>';
								echo '<p><span class="report__chart-legends-score">1-3</span> Good news: It\'s not just new affair, same ol\' story. But can you compromise?</p>';
							echo '</div>';

						echo '</div>';
					echo '</div>';

			    echo '</div>';
		    echo '</div>';

		} else {

			include 'common/report-common-charts-preview.php';

		}

	} else {}

	// ============ Report Articles ============

	if ( $report_part == 'articles' ) {

		include 'common/report-free-limits-start.php';

	    foreach ( $report_content_raw->detail[0]->section as $section ) {

			if ( $report_version == 'free' ) {

				if ( $i == 1 ) break;

			} else {}

			foreach ( $section->paragraph as $paragraph ) {

				$interaspect = $paragraph->interaspect;
				$interaspctchunk = explode(" ",$interaspect);
				$planet1 = strtolower($interaspctchunk[0]);

			}

	        echo'<div class="report__section">';
	        echo '<h2 class="report__section-title"><span>'.$section->title.'</span></h2>';
	  
			foreach ( $section->paragraph as $paragraph ) {

				include 'common/report-common-content-interaspect.php';

				include 'common/report-image-pool-partial.php';

				echo '<div class="report__article">';

				if ( $aspect == 'in' ) {

					echo '<span class="report__article-subtitle"><img src="/wp-content/uploads/images/glyphs/'.$planet1.'_white.png" /><img src="/wp-content/uploads/images/glyphs/'.$planet2.'_white.png" />'.$interaspect.'</span>';

					echo '<h4 class="report__article-title">'.$heading.'</h4>';

					include 'common/report-common-variation-content.php';

				} elseif ( $planet1 == 'no' ) {

					if ( $heading_lower == 'learning to stand up for yourself' ) {

						$interaspect = 'No oppositions';
						$planet1 = 'opposite';
						$interaspect_slug = 'no-opposition';

					} elseif ( $heading_lower == 'no sympathy' ) {

						$interaspect = 'No oppositions';
						$planet1 = 'opposite';
						$interaspect_slug = 'no-oppositions-2';

					} elseif ( $heading_lower == 'my way or the highway' ) {

						$interaspect = 'No oppositions';
						$planet1 = 'opposite';
						$interaspect_slug = 'no-oppositions-3';

					} elseif ( $heading_lower == "what a relief!" ) {

						$interaspect = 'No oppositions';
						$planet1 = 'opposite';
						$interaspect_slug = 'no-oppositions-4';

					}

					echo '<span class="report__article-subtitle"><img src="/wp-content/uploads/images/glyphs/unaspected_white.png" /><img src="/wp-content/uploads/images/glyphs/opposite_white.png" />'.$interaspect.'</span>';

					echo '<h4 class="report__article-title">'.$heading.'</h4>';

					include 'common/report-common-variation-content.php';

				} else {

					echo '<span class="report__article-subtitle"><img src="/wp-content/uploads/images/glyphs/'.$planet1.'_white.png" /><img src="/wp-content/uploads/images/glyphs/'.$aspect.'_white.png" /><img src="/wp-content/uploads/images/glyphs/'.$planet2.'_white.png" />'.$interaspect.'</span>';
					echo '<h4 class="report__article-title">'.$heading.'</h4>';

					include 'common/report-common-variation-content.php';

				}

				echo '</div>';

			}

		    echo '</div>';

			include 'common/report-free-limits-end.php';

	    }

	} else {}

}