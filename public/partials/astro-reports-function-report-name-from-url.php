<?php

global $post;
$report_slug = $post->post_name;

if ( $report_slug == 'personality-profile' ) {

	$output = 'Personality Profile';
	
} elseif ( $report_slug == 'romantic-personality-profile' ) {

	$output = 'Romantic Personality Profile';
	
} elseif ( $report_slug == 'relationship-remedy' ) {

	$output = 'Relationship Remedy';
	
} elseif ( $report_slug == 'break-up-guide' ) {

	$output = 'The Break-Up Guide';
	
} elseif ( $report_slug == 'love-hurts' ) {

	$output = 'Love Hurts: My Relationship Challenges';
	
} elseif ( $report_slug == 'monthly-astrology-forecast' ) {

	$output = 'Monthly Astrology Forecast';
	
} elseif ( $report_slug == 'romantic-forecast' ) {

	$output = 'Romantic Forecast';
	
} elseif ( $report_slug == 'solar-return' ) {

	$output = 'Solar Return';
	
} elseif ( $report_slug == 'romantic-compatibility' ) {

	$output = 'Romantic Compatibility for Couples';
	
} elseif ( $report_slug == 'karmic-compatibility' ) {

	$output = 'Karmic Compatibility';
	
} elseif ( $report_slug == 'friendship-compatibility' ) {

	$output = 'Friendship Compatibility';
	
} elseif ( $report_slug == 'relationship-analysis' ) {

	$output = 'Relationship Analysis for Couples';
	
} elseif ( $report_slug == 'romantic-forecast-couples' ) {

	$output = 'Romantic Forecast for Couples';
	
} else {}