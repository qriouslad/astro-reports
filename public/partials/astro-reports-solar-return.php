<?php

// Solar Return (Birthday Forecast)

$report_name = 'solar-return';

include 'common/report-common-one-person.php';

if ( empty ( $fn ) || empty ( $id ) || empty ( $sd ) ) {

	include 'common/report-not-found.php';

} else {

	include 'common/report-common-site-date-info.php';

	include 'common/report-common-variation.php';

	$birthday_forecast_data_cachekey = 'bf_'.$atts['variation'].'_'.$id.'_'.$datetimeStringPlain;

	$birthday_forecast_data_cached = simplexml_load_string( get_transient( $birthday_forecast_data_cachekey ) );

	if ( false === $birthday_forecast_data_cached ) {

		$url = "https://www.zdki.us/taReportsw/MakeReport.aspx?ReportID=6&ReportVariation=".$report_variation."&ReportFormat=XML&Name1=".$fn."&BirthDate1=".$datetime."&Name2=&BirthDate2=&StartDate=".$datetimeString."&Length=&AccountID=".$accountid."&AppID=astroreports_NUM&MemberID=numerologist1&V=2";

		// defines $data variable with API's response
		include 'common/report-common-http-auth.php';

		$report_content_raw = simplexml_load_string($data);

		set_transient( $birthday_forecast_data_cachekey, $report_content_raw->asXML(), MONTH_IN_SECONDS );

		// echo '<h6>Uncached data is used</h6>';

	} else {

		$report_content_raw = $birthday_forecast_data_cached;

		// echo '<h6>Cached data is used. Cache key: '.$birthday_forecast_data_cachekey.'</h6>';

	}

	$stress = $report_content_raw->summary[0]->stressindex;
	$relaxation = $report_content_raw->summary[0]->relaxationindex;

	$vbar1_score = $stress;
	$vbar2_score = $relaxation;

	// $stress_padded = sprintf("%02d", $stress);
	// $relaxation_padded = sprintf("%02d", $relaxation);

	$summary_index1 = $report_content_raw->summary[0]->chaptersum[0]->index;
	$summary_index2 = $report_content_raw->summary[0]->chaptersum[1]->index;
	$summary_index3 = $report_content_raw->summary[0]->chaptersum[2]->index;
	$summary_index4 = $report_content_raw->summary[0]->chaptersum[3]->index;
	$summary_index5 = $report_content_raw->summary[0]->chaptersum[4]->index;
	$summary_index6 = $report_content_raw->summary[0]->chaptersum[5]->index;

	$hbar1_primary = $summary_index1;
	$hbar2_primary = $summary_index2;
	$hbar3_primary = $summary_index3;
	$hbar4_primary = $summary_index4;
	$hbar5_primary = $summary_index5;
	$hbar6_primary = $summary_index6;

	// $summary_index1_padded = sprintf("%02d", $summary_index1);
	// $summary_index2_padded = sprintf("%02d", $summary_index2);
	// $summary_index3_padded = sprintf("%02d", $summary_index3);
	// $summary_index4_padded = sprintf("%02d", $summary_index4);
	// $summary_index5_padded = sprintf("%02d", $summary_index5);
	// $summary_index6_padded = sprintf("%02d", $summary_index6);

	// echo '<h4 class="report__subtitle">Prepared for '.urldecode( $fn ).', born on '.$dateformatted.'</h4>';

	// 'free' or 'full'
	include 'common/report-common-report-version.php';

	// ============ Report Charts ============

	if ( $report_part == 'charts' ) {

		if ( $report_version == 'full' ) {

		    echo '<div class="report__chart-section">';
				echo '<div class="report__chart-description report__chart-description--full-width">';
				    echo '<h3 class="report__chart-title"><span>Planetary Bar Graphs: Your Year at a Glance</span></h3>';
					echo '<div class="report__chart-description-wrapper">';
				        echo "<p>Your Year at a Glance: what's the unique focus of your year ahead, specifically from a romantic point of view? This handy bar graph reveals your personal themes for the coming twelve months.</p><p>Each year marks a new and different phase of your life. What are your goals for this year? What would you like to learn or accomplish? What opportunities will arise for you this year, at work or in love? Your Solar Return report answers these questions more in depth, but this bar graph provides a quick view into the focus of your next year of life.</p><p>Based on that snapshot of the planets' positions at the exact moment of your Solar Return, the graph reveals the themes of your very own New Year. The more heavily featured planets in your Solar Return chart are the ones that will figure large over the next year. If Mercury makes lots of aspects with other planets, for example, this will be a big year for you in terms of communication and education. You might write a book or go back to school. If Venus makes several significant aspects, this could be a big year for you in love!</p>";
					echo '</div>';
				echo '</div>';
			echo '</div>';

		    echo '<div id="#soulconnection" class="report__chart-section">';
				echo '<h3 class="report__chart-title">Sun - Recognition and Rewards</h3>';
				echo '<div class="report__chart-section-wrapper">';
					echo '<div class="report__chart-description">';
						echo '<div class="report__chart-description-wrapper">';
							echo "<p class=\"report__read-more\">In astrology, the Sun reflects your core self, including your own sense of self as well as recognition and appreciation from others. Recognition, after all, can come from formal, outside sources, such as a raise at work or a trophy for winning the game; from more intimate outside sources, such as your friends or partner learning about who you are and what makes you tick; or from internal sources -- that inner sense of strength and success that enables you to declare strongly, 'This is who I am.' If your Sun line is...Short: This year won't be about public recognition for you. Focus on other pursuits, such as your friendships and working on projects behind the scenes. Medium: Whether you work behind the scenes this year or in the public eye, your pursuits won't be entirely self-focused or -motivated. You'll also focus outward on some person or goal other than your own. Long: Success and personal growth are on your mind this year. Others will admire you for all you're able to accomplish.</p>";
						echo '</div>';
					echo '</div>';
					echo '<div class="report__chart-visuals">';
						echo '<div class="report__chart-animation">';
							include 'common/report-chart-horizontal-one-bar-1.php';
						echo '</div>';
						echo '<div class="report__chart-legends">';
							include 'common/report-chart-horizontal-one-bar-legends.php';
						echo '</div>';
				    echo '</div>';
			    echo '</div>';
		    echo '</div>';

		    echo '<div id="#communicativebond" class="report__chart-section">';
				echo '<h3 class="report__chart-title">Mercury - Mind Expansion</h3>';
				echo '<div class="report__chart-section-wrapper">';
					echo '<div class="report__chart-description">';
						echo '<div class="report__chart-description-wrapper">';
							echo "<p class=\"report__read-more\">Mercury represents your intellect, the way you process information, and your interest in learning, teaching and communicating. Schoolwork, reading, writing, debates, lectures and conversations all fall within Mercury's domain. If your Mercury line is...Short: Either learning and study just aren't the focus for you this year, or you'll find yourself distracted while you're trying to hit the books. Medium: You'll spend some of your time interacting with others and soaking up knowledge. Other times, you'll opt for solitude or relaxation. Not everything in life should be a lesson, after all! Long: You could begin a new course of study this year. Communication will be highlighted, too; you might write a book, or give or attend a series of lectures.</p>";
						echo '</div>';
					echo '</div>';
					echo '<div class="report__chart-visuals">';
						echo '<div class="report__chart-animation">';
							include 'common/report-chart-horizontal-one-bar-2.php';
						echo '</div>';
						echo '<div class="report__chart-legends">';
							include 'common/report-chart-horizontal-one-bar-legends.php';
						echo '</div>';
				    echo '</div>';
			    echo '</div>';
		    echo '</div>';

		    echo '<div id="#lovelink" class="report__chart-section">';
				echo '<h3 class="report__chart-title">Venus - Love and Romance</h3>';
				echo '<div class="report__chart-section-wrapper">';
					echo '<div class="report__chart-description">';
						echo '<div class="report__chart-description-wrapper">';
							echo "<p class=\"report__read-more\">Venus represents love, affection and partnership, the sweetness of romance and the pleasure of togetherness. If your Venus line is...Short: Love affairs won't be a priority this year. Romantic relationships will take a back seat to other pursuits, such as work or study. Loneliness may result, but remember -- it's only for a year. Medium: While romance will be on your mind, it won't consume you. Whew! Enjoy yourself and your love life, and especially enjoy this freedom from romantic obsession. Long: You've got love on the brain this year and you're itching to connect in a romantic way! Just try not to place too much of your self-worth on the state of your love life.</p>";
						echo '</div>';
					echo '</div>';
					echo '<div class="report__chart-visuals">';
						echo '<div class="report__chart-animation">';
							include 'common/report-chart-horizontal-one-bar-3.php';
						echo '</div>';
						echo '<div class="report__chart-legends">';
							include 'common/report-chart-horizontal-one-bar-legends.php';
						echo '</div>';
				    echo '</div>';
			    echo '</div>';
		    echo '</div>';

		    echo '<div id="#passionateattraction" class="report__chart-section">';
				echo '<h3 class="report__chart-title">Mars - Ambition and Desire</h3>';
				echo '<div class="report__chart-section-wrapper">';
					echo '<div class="report__chart-description">';
						echo '<div class="report__chart-description-wrapper">';
							echo "<p class=\"report__read-more\">Mars represents your drive, including the ambitious variety as well as the sexy kind. Whether it's a relationship, a job, a new home or car, if you want it and pursue it, you've got Mars behind you, helping to push you toward your goal. If your Mars line is...Short: There's plenty of time to get ahead, and you know it. This year, striving toward your goals won't be the focus. Don't sweat it; enjoy this chance to relax. Medium: You know what you want and you'll take steps toward getting it this year, but you won't go overboard. Your life is in balance, and you know that success can be a slow, steady train. Long: Winning is everything for you this year, and you could go far in the pursuit of your goals. Just take care not to bulldoze over anyone that gets in your way!</p>";
						echo '</div>';
					echo '</div>';
					echo '<div class="report__chart-visuals">';
						echo '<div class="report__chart-animation">';
							include 'common/report-chart-horizontal-one-bar-4.php';
						echo '</div>';
						echo '<div class="report__chart-legends">';
							include 'common/report-chart-horizontal-one-bar-legends.php';
						echo '</div>';
				    echo '</div>';
			    echo '</div>';
		    echo '</div>';

		    echo '<div id="#sharedvalues" class="report__chart-section">';
				echo '<h3 class="report__chart-title">Jupiter - Travel and Finding Truth</h3>';
				echo '<div class="report__chart-section-wrapper">';
					echo '<div class="report__chart-description">';
						echo '<div class="report__chart-description-wrapper">';
							echo "<p class=\"report__read-more\">Jupiter is all about expansion and philosophy. This planet pushes you to broaden your experience as well as your intellect, to push further beyond your own boundaries than ever before. If your Jupiter line is...Short: Whether it's due to tight funds or a jam-packed schedule, the most you may be able to manage this year is a few weekend trips or other short jaunts. Medium: You might fit in a good vacation or two this year. In the off-season, or if travel just isn't possible, remember the mind-expanding benefits of a great documentary film or nonfiction book! Long: This is the year to broaden your horizons in a big way. You may take a long trip to someplace exotic that expands your consciousness, or this could occur through friendship with someone from a different culture.</p>";
						echo '</div>';
					echo '</div>';
					echo '<div class="report__chart-visuals">';
						echo '<div class="report__chart-animation">';
							include 'common/report-chart-horizontal-one-bar-5.php';
						echo '</div>';
						echo '<div class="report__chart-legends">';
							include 'common/report-chart-horizontal-one-bar-legends.php';
						echo '</div>';
				    echo '</div>';
			    echo '</div>';
		    echo '</div>';

		    echo '<div id="#longtermpotential" class="report__chart-section">';
				echo '<h3 class="report__chart-title">Saturn - Responsibility and Restriction</h3>';
				echo '<div class="report__chart-section-wrapper">';
					echo '<div class="report__chart-description">';
						echo '<div class="report__chart-description-wrapper">';
							echo "<p class=\"report__read-more\">Saturn acts as your conscience and your stern taskmaster. This planet reminds you that you'd better stop goofing off, start working hard and take care of those responsibilities. If your Saturn line is...Short: You won't feel hemmed in this year by duties or self-discipline. Instead, you've got the freedom you need to go where you want to go. Of course, you may not get a lot accomplished...oh well, there's always next year. Medium: You'll have to keep up with the important things -- job, bills, family -but don't worry. You'll still be able to include plenty of carefree fun in your schedule! Long: Your responsibilities will weigh heavily on you, as if you're Atlas, carrying the world on your shoulders. There's a lot on your plate, but remember, it's only for a year. Buckle down and work hard.</p>";
						echo '</div>';
					echo '</div>';
					echo '<div class="report__chart-visuals">';
						echo '<div class="report__chart-animation">';
							include 'common/report-chart-horizontal-one-bar-6.php';
						echo '</div>';
						echo '<div class="report__chart-legends">';
							include 'common/report-chart-horizontal-one-bar-legends.php';
						echo '</div>';
				    echo '</div>';
			    echo '</div>';
		    echo '</div>';

		    echo '<div class="report__chart-section">';
				echo '<h3 class="report__chart-title"><span>Your Stress and Relaxation Ratings</span></h3>';
				echo '<div id="#sun" class="report__chart-section-wrapper">';
					echo '<div class="report__chart-description">';
						echo '<div class="report__chart-description-wrapper">';
							echo "<p>On your birthday, everyone wishes you a wonderful year ahead. But what's it really going to be like? It can't all be a barrel of monkeys. We've created these meters to give you that at-a-glance view you need of your year ahead.</p><p>The Stress meter measures the potential for obstacles in the coming year. Whether it's one trouble spot after another or you've got a free ticket to Easy Street, remember that every hurdle life tosses up in your path presents a lesson. That's what life is all about -- jumping through hoops, growing and learning.</p><p>The Relaxation meter measures the easy times ahead -- those days and weeks when you'll have a smile on your face and a spring in your step regardless of what's going on around you. Though you may think you want a high Relaxation score and a low Stress one, it's actually best if you come out somewhere in the middle on both meters. That way, your year will be fun, interesting and complex -- just like you!</p><p>Finally, remember: If you don't like what you see, it'll only last a year! Next year, this energy will drain away like water in the tub. Time to cast a new Solar Return!</p>";
						echo '</div>';
					echo '</div>';
					echo '<div class="report__chart-visuals">';
						echo '<div class="report__chart-animation">';
							include 'common/report-chart-vertical-two-bars.php';
						echo '</div>';
						echo '<div class="report__chart-legends">';
							echo '<div class="report__chart-legends-half-left">';
							    echo '<h4 class="report__chart-legends-title report__chart-legends-title--half-left"">Stress = '.$stress.'</h4>';
								echo '<p><span class="report__chart-legends-score">7-10</span>: One thing after another and no end in sight</p>';
								echo '<p><span class="report__chart-legends-score">4-6</span>: Some bumps on the road, but nothing you can\'t handle</p>';
								echo '<p><span class="report__chart-legends-score">1-3</span>: Living on Easy Street!</p>';
							echo '</div>';
							echo '<div class="report__chart-legends-half-right">';
							    echo '<h4 class="report__chart-legends-title report__chart-legends-title--half-right">Relaxation = '.$relaxation.'</h4>';
								echo '<p><span class="report__chart-legends-score">7-10</span>: Even when life gets hectic, you still know how to chill</p>';
								echo '<p><span class="report__chart-legends-score">4-6</span>: You\'ll work hard and play hard too</p>';
								echo '<p><span class="report__chart-legends-score">1-3</span>: Schedule a massage - you\'re going to need it!</p>';
							echo '</div>';
						echo '</div>';
				    echo '</div>';
			    echo '</div>';
		    echo '</div>';

			include 'common/report-common-read-more-less-js.php';

		} else {

			include 'common/report-common-charts-preview.php';

		}

	} else {}

	// ============ Report Articles ============

	if ( $report_part == 'articles' ) {

		include 'common/report-free-limits-start.php';

	    foreach ($report_content_raw->detail[0]->section as $section) {

			if ( $report_version == 'free' ) {

				if ( $i == $limit ) break;

			} else {}

			echo '<div class="report__section">';
			echo '<h2 class="report__section-title"><span>'.$section->title.'</span></h2>';

			if ( $section->title != 'Broader Influences' ) {

				foreach ($section->paragraph as $paragraph) {

					include 'common/report-common-content-interaspect.php';

					include 'common/report-image-pool-partial.php';

					include 'common/report-common-birthday-forecast.php';

		  		}

			} else {

				$interaspect_content = array();

				foreach ($section->paragraph as $paragraph) {

					include 'common/report-common-content-interaspect.php';

					if ( $planet1 == 'uranus' ) {

						$planet_sequence = 1;

					} elseif ( $planet1 == 'neptune' ) {

						$planet_sequence = 2;

					} elseif ( $planet1 == 'pluto' ) {

						$planet_sequence = 3;

					} else {}

					$additional_content = array(
							'planet_sequence' => $planet_sequence,
							'planet1' => $planet1,
							'aspect' => $aspect,
							'planet2' => $planet2,
							'interaspect' => $interaspect,
							'heading' => (string)$heading,
							'text' => (string)$text,
					);

					$interaspect_content[] = $additional_content;

				}

				// https://stackoverflow.com/a/19454643			
				usort($interaspect_content, function ($array1, $array2) {
				    return $array1['planet_sequence'] <=> $array2['planet_sequence'];
				});

				for( $i=0; $i<count( $interaspect_content ); $i++ ) {

					include 'common/report-image-pool-partial.php';

					$planet1 = $interaspect_content[$i]['planet1'];
					$aspect = $interaspect_content[$i]['aspect'];
					$planet2 = $interaspect_content[$i]['planet2'];
					$interaspect = $interaspect_content[$i]['interaspect'];
					$heading = $interaspect_content[$i]['heading'];
					$text = $interaspect_content[$i]['text'];

					include 'common/report-common-birthday-forecast.php';

				}

			}

	      	echo '</div>';

			include 'common/report-free-limits-end.php';

		}

	} else {}

}