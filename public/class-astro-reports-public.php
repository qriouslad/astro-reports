<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://bowo.io
 * @since      1.0.0
 *
 * @package    Astro_Reports
 * @subpackage Astro_Reports/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Astro_Reports
 * @subpackage Astro_Reports/public
 * @author     Wibowo Sulistio <wibowosulistio@gmail.com>
 */
class Astro_Reports_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Astro_Reports_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Astro_Reports_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		// https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css
		// wp_enqueue_style( $this->plugin_name.'-materialize', 'https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css', array(), $this->version, 'all' );

		wp_enqueue_style( $this->plugin_name.'-materialize', plugin_dir_url( __FILE__ ) . 'css/astro-reports-materialize-trimmed.css', array(), $this->version, 'all' );

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/astro-reports-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Astro_Reports_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Astro_Reports_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		// https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js
		wp_enqueue_script( $this->plugin_name.'-materialize', 'https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js', array( 'jquery' ), $this->version, false );

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/astro-reports-public.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 * Function to display the various astrology reports for one or two person
	 *
	 * @since 1.0.0
	 */

	public function astrology_reports( $atts ) {

		$atts = shortcode_atts(
			array(
				'type' => '',
				'variation' => 'full',
				'part' => 'articles',
			),
			$atts
		);

		ob_start();
		
		if ( $atts['type'] == 'personality-profile' ) {

			include 'partials/astro-reports-personality-profile.php';
			
		} elseif ( $atts['type'] == 'romantic-personality' ) {

			include 'partials/astro-reports-romantic-personality.php';
			
		} elseif ( ( $atts['type'] == 'relationship-remedy' ) || ( $atts['type'] == 'relationship-wrongs' ) ) {

			include 'partials/astro-reports-relationship-remedy.php';
			
		} elseif ( $atts['type'] == 'romantic-forecast' ) {

			include 'partials/astro-reports-romantic-forecast.php';

		} elseif ( ( $atts['type'] == 'monthly-astrology-forecast' ) || ( $atts['type'] == 'life-forecast' ) ) {

			include 'partials/astro-reports-monthly-astrology-forecast.php';

		} elseif ( ( $atts['type'] == 'solar-return' ) || ( $atts['type'] == 'birthday-forecast' ) ) {

			include 'partials/astro-reports-solar-return.php';
			
		} elseif ( $atts['type'] == 'friendship-compatibility' ) {

			include 'partials/astro-reports-friendship-compatibility.php';
			
		} elseif ( $atts['type'] == 'love-hurts' ) {

			include 'partials/astro-reports-love-hurts.php';
			
		} elseif ( ( $atts['type'] == 'break-up-guide' ) || ( $atts['type'] == 'me-and-my-ex' ) ) {

			include 'partials/astro-reports-break-up-guide.php';
			
		} elseif ( $atts['type'] == 'romantic-compatibility' ) {

			include 'partials/astro-reports-romantic-compatibility.php';
			
		} elseif ( ( $atts['type'] == 'karmic-compatibility' ) || ( $atts['type'] == 'karma-destiny' ) ) {

			include 'partials/astro-reports-karmic-compatibility.php';
			
		} elseif ( ( $atts['type'] == 'relationship-potential' ) || ( $atts['type'] == 'relationship-analysis' ) ) {

			include 'partials/astro-reports-relationship-potential.php';
			
		} elseif ( $atts['type'] == 'romantic-forecast-couples' ) {

			include 'partials/astro-reports-romantic-forecast-couples.php';
			
		} else {}

		$output = ob_get_clean();

		return $output;

	}

	/**
	 * Function to display various astrology forms built on the PHP Form Builder class https://codecanyon.net/item/php-form-builder/8790160
	 *
	 * @since 1.0.0
	 */

	public function astrology_forms( $atts ) {

		$atts = shortcode_atts(
			array(
				'type' => '',
			),
			$atts
		);

		// https://www.phpformbuilder.pro/documentation/class-doc.php#overview
		// PHP Form Builder files is inside the 'vendor' folder in plugin root
		// The Form class is included from /includes/class-astro-reports.php

		ob_start();
		
		if ( $atts['type'] == 'personal-info' ) {

			include 'partials/astro-report-form-personal-info.php';

		} else {}

		$output = ob_get_clean();

		return $output;

	}

	/**
	 * Register the various shortcodes for displaying astrological reports
	 * @since 1.0.0
	 */

	public function register_shortcodes() {

		add_shortcode( 'astroreport', array( $this, 'astrology_reports' ) );
		add_shortcode( 'astroform', array( $this, 'astrology_forms' ) );

	}

	/**
	 * Function to display personal data from form POST data or URL GET parameters
	 *
	 * @since 1.0.0
	 */

	// https://wordpress.stackexchange.com/a/116331

	public static function get_personal_data( $type ) {

		include 'partials/astro-reports-function-personal-data.php';

		return $output;

	}

	/**
	 * Function to output report name based on page url
	 *
	 * @since 1.0.0
	 */

	function get_report_name_from_url() {

		include 'partials/astro-reports-function-report-name-from-url.php';
			
		return $output;
		
	}

}


/**
 * Making functions available throughout the WP application, including Oxygen Builder
 * 
 * @since 1.0.0
 */

// https://wordpress.stackexchange.com/a/116331

if ( ! function_exists( 'get_personal_data' ) ) {

    function personal_data( $type ) {

       echo Astro_Reports_Public::get_personal_data( $type );

    }

}

if ( ! function_exists( 'get_report_name_from_url' ) ) {

    function report_name_from_url() {

       echo Astro_Reports_Public::get_report_name_from_url();

    }

}