<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://astrology.tv
 * @since             1.3.0
 * @package           Astro_Reports
 *
 * @wordpress-plugin
 * Plugin Name:       Astro Reports
 * Plugin URI:        https://astrology.tv
 * Description:       This plugin generates astrological reports from external API data
 * Version:           1.1.0
 * Author:            Wibowo Sulistio
 * Author URI:        https://bowo.io
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       astro-reports
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'ASTRO_REPORTS_VERSION', '1.3.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-astro-reports-activator.php
 */
function activate_astro_reports() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-astro-reports-activator.php';
	Astro_Reports_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-astro-reports-deactivator.php
 */
function deactivate_astro_reports() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-astro-reports-deactivator.php';
	Astro_Reports_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_astro_reports' );
register_deactivation_hook( __FILE__, 'deactivate_astro_reports' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-astro-reports.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_astro_reports() {

	$plugin = new Astro_Reports();
	$plugin->run();

}
run_astro_reports();
