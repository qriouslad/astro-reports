<?php

/**
 * Fired during plugin activation
 *
 * @link       https://bowo.io
 * @since      1.0.0
 *
 * @package    Astro_Reports
 * @subpackage Astro_Reports/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Astro_Reports
 * @subpackage Astro_Reports/includes
 * @author     Wibowo Sulistio <wibowosulistio@gmail.com>
 */
class Astro_Reports_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
