<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://bowo.io
 * @since      1.0.0
 *
 * @package    Astro_Reports
 * @subpackage Astro_Reports/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Astro_Reports
 * @subpackage Astro_Reports/includes
 * @author     Wibowo Sulistio <wibowosulistio@gmail.com>
 */
class Astro_Reports_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
